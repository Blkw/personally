import React, { Component } from "react";

class Feed extends Component {
  render() {
    return (
      <div>
        <div className="feed-container">
          <div className="post">
            <div className="author-section" />
            <div className="post-content" />
            <div className="actions">
              <div className="actions-tab">Like</div>
              <div className="actions-tab">Comment</div>
              <div className="actions-tab">Repost</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Feed;
