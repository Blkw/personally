export const create = (userId, token, post) => {
  return fetch(`${process.env.REACT_APP_API_URL}/feed/new/${userId}/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`
    },
    body: post
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const feedFollow = async (newsfeedId, token, followId) => {
  console.log("newsfeedId in the follow fuction.apiFeed: ", newsfeedId);
  console.log("followId in the follow fuction.apiFeed: ", followId);

  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/feed/follow`,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({ newsfeedId, followId })
      }
    );
    return response.json();
  } catch (err) {
    return console.log(err);
  }
};

export const feedUnfollow = (newsfeedId, token, unfollowId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/feed/unfollow`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({ newsfeedId, unfollowId })
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const read = (feedId, token) => {
  return fetch(`${process.env.REACT_APP_API_URL}/feed/${feedId}`, {
    method: "GET",
    headers: {
      Accept: "appliacation/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
