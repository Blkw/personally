import React from "react";
import { Route, Switch } from "react-router-dom";
import Menu from "./core/Menu";
import Home from "./core/Home";
import Signup from "./user/Signup";
import Signin from "./user/Signin";
import Profile from "./user/Profile";
import EditProfile from "./user/EditProfile";
import Users from "./user/Users";
import PrivateRoute from "./auth/PrivateRoute";
import EditPost from "./post/EditPost";
import SinglePost from "./post/SinglePost";
import TagResults from "./tags/TagResults";

const MainRouter = () => (
  <div id="page-wrapper">
    <Menu />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/signin" component={Signin} />
      <Route exact path="/user/:userId" component={Profile} />
      <Route exact path="/post/:postId" component={SinglePost} />
      <PrivateRoute exact path="/user/edit/:userId" component={EditProfile} />
      <PrivateRoute exact path="/post/edit/:postId" component={EditPost} />
      <Route exact path="/users/" component={Users} />
      <Route exact path="/posts/search/:searchTerm" component={TagResults} />
    </Switch>
  </div>
);
export default MainRouter;
