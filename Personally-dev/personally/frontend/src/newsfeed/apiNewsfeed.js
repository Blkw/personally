export const create = (userId, token, post) => {
  return fetch(`${process.env.REACT_APP_API_URL}/newsfeed/new/${userId}/`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`
    },
    body: post
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const follow = async (userId, token, followId) => {
  console.log("userId in the follow fuction.apinewsfeed: ", userId);
  console.log("followId in the follow fuction.apinewsfeed: ", followId);

  try {
    const response = await fetch(
      `${process.env.REACT_APP_API_URL}/newsfeed/follow`,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({ userId, followId })
      }
    );
    return response.json();
  } catch (err) {
    return console.log(err);
  }
};

export const unfollow = (userId, token, unfollowId) => {
  return fetch(`${process.env.REACT_APP_API_URL}/newsfeed/unfollow`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({ userId, unfollowId })
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const read = (feedId, token) => {
  return fetch(`${process.env.REACT_APP_API_URL}/newsfeed/${feedId}`, {
    method: "GET",
    headers: {
      Accept: "appliacation/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const newsfeedsByUser = (userId, token) => {
  return fetch(`${process.env.REACT_APP_API_URL}/newsfeeds/by/${userId}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  })
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const postsByNewsfeed = (feedId, page, token) => {
  return fetch(
    `${process.env.REACT_APP_API_URL}/posts-by-newsfeed/${feedId}/?page=${page}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  )
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
