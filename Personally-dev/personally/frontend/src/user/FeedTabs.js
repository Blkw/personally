import React, { Component } from "react";
import DefaultProfile from "../assets/images/avatar.png";
import { Link } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { isAuthenticated } from "../auth";
import PostActionsPopup from "../assets/components/PostActionsPopup";
//import Masonry from "react-masonry-component";
import { postsByFeed } from "../post/apiPost";
import CreateFeed from "../assets/components/CreateFeed";
import PostActions from "../assets/components/PostActions";
import Linkify from "linkifyjs/react";
import ReactTinyLink from "react-tiny-link";
import CustomVideo from "../assets/components/CustomVideo";
import CustomVideoMp4 from "../assets/components/CustomVideoMp4";
import { read } from "../feed/apiFeed";
import ChooseFollow from "../assets/components/ChooseFollow";
import { newsfeedsByUser } from "../newsfeed/apiNewsfeed";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight as arrow } from "@fortawesome/free-solid-svg-icons";
import Masonry from "react-masonry-css";

const breakpointColumnsObj = {
  default: 2,
  500: 1
};

//styling
const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};

const postImage = {
  width: "100%",
  height: "auto",
  borderRadius: "10px",
  margin: "0"
};

const text = {
  marginTop: "0",
  marginBottom: "0"
};

const tags = {
  marginTop: "15px",
  marginBottom: "0",
  color: "#D2D7DC"
};

const masonryOptions = {
  transitionDuration: 1
};

const imagesLoadedOptions = { background: ".my-bg-image-el" };

export default class FeedTabs extends Component {
  constructor() {
    super();
    this.state = {
      feed: { following: [], followers: [] },
      redirectToSignin: false,
      following: false,
      feedsPosts: [],
      selectedFeed: null,
      newsfeeds: [],
      displayInRow: true
    };
  }

  componentDidMount() {
    this.getUsersNewsfeeds(isAuthenticated().user._id);
  }

  getUsersNewsfeeds = userId => {
    const token = isAuthenticated().token;
    newsfeedsByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ newsfeeds: data });
      }
    });
  };

  loadFeed = feedId => {
    this.init(feedId);
    this.setState({ feedsPosts: [] });
    const token = isAuthenticated().token;
    postsByFeed(feedId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ selectedFeed: feedId });
        this.setState({ feedsPosts: data });
      }
    });
  };

  init = feedId => {
    const token = isAuthenticated().token;
    read(feedId, token).then(data => {
      if (data.error) {
        this.setState({ redirectToSignin: true });
        console.log("Error.");
      } else {
        let following = this.checkFollow(data);
        this.setState({ feed: data, following });
      }
    });
  };

  seeMore = post => {
    if (post.text.length > 400) {
      return (
        <Link className="comments-error" to={`/post/${post._id}`}>
          <br />
          <br />
          See more...
        </Link>
      );
    }
  };

  allPosts = () => {
    const { posts } = this.props;

    return (
      <>
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {posts.map((post, i) => {
            const posterId = post.postedBy ? post.postedBy._id : "";
            const posterName = post.postedBy
              ? post.postedBy.username
              : "Unknown";
            return (
              <div key={i} className="feed-post search">
                <div className="author-section">
                  <Link to={`/user/${posterId}`}>
                    <div className="circle-photo-list">
                      <img
                        style={profileImage}
                        src={`${process.env.REACT_APP_API_URL}/user/photo/${posterId}`}
                        onError={i => (i.target.src = `${DefaultProfile}`)}
                        alt={posterName}
                      />
                    </div>
                  </Link>
                  <Link to={`/user/${posterId}`}>
                    <p className="poster">{posterName}</p>
                  </Link>
                  <p className="onto">
                    <FontAwesomeIcon icon={arrow} />
                  </p>
                  <Link to={`/user/${posterId}`}>
                    <p className="poster">{post.postsFeed.title}</p>
                  </Link>
                  {/* {this.dateTime(post.created)} */}
                </div>
                <div className="post-content">
                  <Link to={`/post/${post._id}`}>
                    <img
                      alt={post.text}
                      style={postImage}
                      src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
                      onError={i => (i.target.style.display = "none")}
                    />
                  </Link>
                  <p className="list-group-item-heading" style={text}>
                    <Linkify>{post.text.substring(0, 400)}</Linkify>
                    {/*  makes the maxiumum text on the feed 400 chars */}
                  </p>
                  {this.renderLinkPreview(post.text)}
                  {this.seeMore(post)}
                  <p className="list-group-item-text">
                    {this.renderTags(post)}
                  </p>
                </div>
                <PostActions post={post} />
              </div>
            );
          })}
        </Masonry>
      </>
    );
  };

  renderTags = post => {
    if (post.tags[0]) {
      var tagsArray = post.tags[0].split(",");
      console.log(tagsArray);
      return (
        <>
          {tagsArray.map((tag, i) => {
            return (
              <Link to={`/posts/search/${tag}`} key={i}>
                #{tag}
              </Link>
            );
          })}
        </>
      );
    } else {
      return null;
    }
  };

  isEmpty = obj => {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  };

  renderLinkPreview = text => {
    var matches = text.match(/(((https?:\/\/)|(www\.))[^\s]+)/g);

    if (matches != null) {
      var thisLink = matches[0];

      if (thisLink.search(/^http[s]?\:\/\//) == -1) {
        thisLink = "http://" + thisLink;
      }

      var hasyoutube = thisLink.includes("youtube");
      var hasVimeo = thisLink.includes("vimeo");
      var notFriendly = thisLink.includes(".mp4");

      if (hasyoutube || hasVimeo) {
        return <CustomVideo thisLink={thisLink} />;
      } else if (notFriendly) {
        return <CustomVideoMp4 thisLink={thisLink} />;
      } else {
        return (
          <ReactTinyLink
            cardSize="large"
            showGraphic={true}
            maxLine={3}
            minLine={1}
            url={`${thisLink}`}
          />
        );
      }
    } else return false;
  };

  checkFollow = feed => {
    const jwt = isAuthenticated();
    const match = feed.followers.find(follower => {
      //one id (user) has many other ids ( followers ) and vice versa
      return follower._id === jwt.user._id;
    });
    return match;
  };

  clickFollowButton = callApi => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;
    callApi(userId, token, this.state.selectedFeed).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({ feed: data, following: !this.state.following });
      }
    });
  };

  feedClickedFollow = data => {
    this.setState({ feed: data, following: !this.state.following });
  };

  feededPosts = () => {
    var posts = this.state.feedsPosts;
    if (!this.isEmpty(posts)) {
      return (
        <>
          {this.checkUser()}

          <Masonry
            breakpointCols={breakpointColumnsObj}
            className="my-masonry-grid"
            columnClassName="my-masonry-grid_column"
          >
            {posts.map((post, i) => {
              const posterId = post.postedBy ? post.postedBy._id : "";
              const posterName = post.postedBy
                ? post.postedBy.username
                : "Unknown";
              return (
                <div key={i} className="feed-post search">
                  <div className="author-section">
                    <Link to={`/user/${posterId}`}>
                      <div className="circle-photo-list">
                        <img
                          style={profileImage}
                          src={`${process.env.REACT_APP_API_URL}/user/photo/${posterId}`}
                          onError={i => (i.target.src = `${DefaultProfile}`)}
                          alt={posterName}
                        />
                      </div>
                    </Link>
                    <Link to={`/user/${posterId}`}>
                      <p className="poster">{posterName}</p>
                    </Link>
                    <p className="onto">
                      <FontAwesomeIcon icon={arrow} />
                    </p>
                    <Link to={`/user/${posterId}`}>
                      <p className="poster">{post.postsFeed.title}</p>
                    </Link>
                    {/* {this.dateTime(post.created)} */}
                  </div>
                  <div className="post-content">
                    <Link to={`/post/${post._id}`}>
                      <img
                        alt={post.text}
                        style={postImage}
                        src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
                        onError={i => (i.target.style.display = "none")}
                      />
                    </Link>
                    <p className="list-group-item-heading" style={text}>
                      <Linkify>{post.text.substring(0, 400)}</Linkify>
                      {/*  makes the maxiumum text on the feed 400 chars */}
                    </p>
                    {this.renderLinkPreview(post.text)}
                    {this.seeMore(post)}
                    <p className="list-group-item-text">
                      {this.renderTags(post)}
                    </p>
                  </div>
                  <PostActions post={post} />
                </div>
              );
            })}
          </Masonry>
        </>
      );
    } else {
      return <p>This thread doesn't have any posts yet :(</p>;
    }
  };

  shitcode = () => {
    if (isAuthenticated().user._id === this.props.user._id) {
      return <CreateFeed />;
    } else {
      return null;
    }
  };

  checkUser = () => {
    if (isAuthenticated().user._id != this.props.user._id) {
      return (
        <ChooseFollow
          selectedfeed={this.state.feed}
          isFollowingThisFeed={this.state.following}
          newsfeeds={this.state.newsfeeds}
        />
      );
    } else {
      return null;
    }
  };

  render() {
    //will need to loop through all tabs
    //create tab popup

    const { feeds } = this.props;

    return (
      <div>
        <Tabs>
          <TabList id="feed-nav">
            <Tab className="feed-nav-item">All posts</Tab>
            {feeds.map((feed, i) => {
              return (
                <Tab
                  onClick={() => {
                    this.loadFeed(feed._id);
                  }}
                  className="feed-nav-item"
                  key={i}
                >
                  {feed.title}
                </Tab>
              );
            })}
            {feeds ? this.shitcode() : "Loading..."}
          </TabList>
          <div className="feedTabs">
            <TabPanel>{this.allPosts()}</TabPanel>
            {feeds.map((feed, i) => {
              return <TabPanel key={i}>{this.feededPosts()}</TabPanel>;
            })}
          </div>
        </Tabs>
      </div>
    );
  }
}
