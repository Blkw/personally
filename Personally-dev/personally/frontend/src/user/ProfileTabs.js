import React, { Component } from "react";
import DefaultProfile from "../assets/images/avatar.png";
import { Link } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { isAuthenticated } from "../auth";
import PostActionsPopup from "../assets/components/PostActionsPopup";
import FeedTabs from "./FeedTabs";

//styling
const cardStyle = {
  borderBottom: "1px solid lightgrey"
};

const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};

const pFix = {
  marginTop: "0",
  marginBottom: "0"
};

class ProfileTabs extends Component {
  followersCount = () => {
    const { followers } = this.props;

    return "followers " + followers.length;
  };
  followingCount = () => {
    const { following } = this.props;

    return "following " + following.length;
  };

  userFollowers = () => {
    const { followers } = this.props;

    return (
      <div className="main-content">
        <h3 className="react-tab-headers">followers</h3>
        <ul>
          {followers.map((user, i) => (
            <Link
              to={`/user/${user._id}`}
              className="list-group-item"
              style={cardStyle}
              key={i}
            >
              <div className="list-group-item-inline">
                <div className="circle-photo-list">
                  <img
                    style={profileImage}
                    src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}`}
                    onError={i => (i.target.src = `${DefaultProfile}`)}
                    alt={user.username}
                  />
                </div>
                <div className="user-info">
                  <p className="list-group-item-heading" style={pFix}>
                    {user.username}
                  </p>
                  <p className="list-group-item-text" style={pFix}>
                    {user.firstname} {user.lastname}
                  </p>
                </div>
              </div>
            </Link>
          ))}
        </ul>
      </div>
    );
  };

  userFollowing = () => {
    const { following } = this.props;

    return (
      <div className="main-content">
        <h3 className="react-tab-headers">following</h3>
        <ul>
          {following.map((user, i) => (
            <Link
              to={`/user/${user._id}`}
              className="list-group-item"
              style={cardStyle}
              key={i}
            >
              <div className="list-group-item-inline">
                <div className="circle-photo-list">
                  <img
                    style={profileImage}
                    src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}`}
                    onError={i => (i.target.src = `${DefaultProfile}`)}
                    alt={user.username}
                  />
                </div>
                <div className="user-info">
                  <p className="list-group-item-heading" style={pFix}>
                    {user.username}
                  </p>
                  <p className="list-group-item-text" style={pFix}>
                    {user.firstname} {user.lastname}
                  </p>
                </div>
              </div>
            </Link>
          ))}
        </ul>
      </div>
    );
  };

  posts = () => {
    const { posts, feeds, user } = this.props;

    return (
      <>
        <FeedTabs user={user} posts={posts} feeds={feeds} />
      </>
    );
  };

  about = () => {
    return (
      <>
        <h3 className="react-tab-headers">About</h3>
        <p>This section is coming soon...</p>
      </>
    );
  };

  render() {
    return (
      <div>
        <Tabs>
          <div id="profile-header-bottom">
            <TabList id="profile-nav">
              <Tab className="nav-item">Posts</Tab>
              <Tab className="nav-item">About</Tab>
              <Tab className="nav-item">{this.followersCount()} </Tab>
              <Tab className="nav-item">{this.followingCount()} </Tab>
            </TabList>
          </div>
          <div className="tabs">
            <TabPanel>{this.posts()}</TabPanel>
            <TabPanel>{this.about()}</TabPanel>
            <TabPanel>{this.userFollowers()}</TabPanel>
            <TabPanel>{this.userFollowing()}</TabPanel>
          </div>
        </Tabs>
      </div>
    );
  }
}
export default ProfileTabs;
