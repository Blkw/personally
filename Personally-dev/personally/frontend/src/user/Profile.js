import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { Redirect, Link } from "react-router-dom";
import { read, feedsByUser } from "./apiUser";
import DeleteUser from "./DeleteUser";
import DefaultProfile from "../assets/images/avatar.png";
import FollowProfileButton from "./FollowProfileButton";
import ProfileTabs from "./ProfileTabs";
import { listByUser, postsByFeed } from "../post/apiPost";
import { Header, Modal } from "semantic-ui-react";
import { newsfeedsByUser } from "../newsfeed/apiNewsfeed";

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      user: { following: [], followers: [] },
      redirectToSignin: false,
      following: false,
      error: "",
      posts: [],
      feeds: [],
      feedsPosts: [],
      newsfeeds: [],
      selectedNewsFeed: ""
    };
  }

  getCoverPhoto() {
    let user = this.state.user;
    return user.CoverPhoto;
  }

  checkFollow = user => {
    const jwt = isAuthenticated();
    const match = user.followers.find(follower => {
      //one id (user) has many other ids ( followers ) and vice versa
      return follower._id === jwt.user._id;
    });
    return match;
  };

  clickFollowButton = callApi => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;

    callApi(userId, token, this.state.user._id).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({
          user: data,
          following: !this.state.following
        });
      }
    });
  };

  checkUser() {
    let user = this.state.user;
  }

  init = userId => {
    const token = isAuthenticated().token;

    read(userId, token).then(data => {
      if (data.error) {
        this.setState({ redirectToSignin: true });
        console.log("Error.");
      } else {
        let following = this.checkFollow(data);

        this.setState({ user: data, following });
        this.loadPosts(data._id);
        this.loadFeeds(data._id);
        this.getUsersNewsfeeds(isAuthenticated().user._id);
      }
    });
  };

  getUsersNewsfeeds = userId => {
    const token = isAuthenticated().token;
    newsfeedsByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ newsfeeds: data });
      }
    });
  };

  loadPosts = userId => {
    const token = isAuthenticated().token;
    listByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ posts: data });
      }
    });
  };

  loadFeeds = feedId => {
    const token = isAuthenticated().token;
    feedsByUser(feedId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ feeds: data });
      }
    });
  };

  //Courtesy of Alex Zenin on july 10 2019
  shitcode = () => {
    return isAuthenticated().user &&
      isAuthenticated().user._id === this.state.user._id ? (
      <div className="d-inline-block mt-5 mb-5" />
    ) : (
      this.getFollowButton()
    );
  };

  getFollowButton = () => {
    return (
      <Modal
        on="click"
        trigger={<button className="follow-settings">Follow settings</button>}
        centered={false}
      >
        <Modal.Header className="createFeed">Create a feed</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Header>
              Which newsfeed would you like "{this.state.user.username}" to show
              in?
            </Header>
            <form onSubmit={this.onSubmit}>{this.renderNewsfeedsList()}</form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  };

  renderFollowButton = id => {
    let userId = this.state.user._id;
    let newsfeeds = this.state.newsfeeds;
    let isFollowing = false;

    for (let i = 0; i < newsfeeds.length; i++) {
      if (newsfeeds[i]._id == id) {
        let following = newsfeeds[i].following;
        for (let x = 0; x < following.length; x++) {
          if (newsfeeds[i].following[x] == userId) {
            isFollowing = true;
          }
        }
      }
    }
    return (
      <FollowProfileButton
        type="button"
        followType="user"
        following={isFollowing}
        newsfeedId={id}
        user={userId}
        onClick={this.preventDefault}
      />
    );
  };
  preventDefault = event => {
    event.preventDefault();
  };
  onSubmit(event) {
    event.preventDefault();
  }

  renderNewsfeedsList = () => {
    const { newsfeeds } = this.state;

    return (
      <>
        <ul className="follow-actions-list">
          {newsfeeds.map((newsfeed, i) => {
            return (
              <li
                className="follow-actions-list-item"
                key={i}
                value={`${newsfeed._id}`}
              >
                <p className="follow-actions-newfeed-name">{newsfeed.title}</p>
                {this.renderFollowButton(newsfeed._id)}
              </li>
            );
          })}
        </ul>
      </>
    );
  };

  componentDidMount() {
    const userId = this.props.match.params.userId;
    this.init(userId);
  }
  componentWillReceiveProps(props) {
    const userId = props.match.params.userId;
    this.init(userId);

    if (props.location.pathname !== this.props.location.pathname) {
      window.location.reload();
    }
  }
  renderUsersName() {
    const thisUser = this.state.user;
    if (this.state.user) {
      return (
        <p>
          {thisUser ? thisUser.firstname + " " : "loading"}{" "}
          {thisUser ? thisUser.lastname + " " : "loading"}
        </p>
      );
    }
  }

  render() {
    const { redirectToSignin, user, posts, feeds } = this.state;

    const photoUrl = user._id
      ? `${process.env.REACT_APP_API_URL}/user/photo/${
          user._id
        }?${new Date().getTime()}`
      : DefaultProfile;

    const coverPhotoUrl = user._id
      ? `${process.env.REACT_APP_API_URL}/user/coverPhoto/${
          user._id
        }?${new Date().getTime()}`
      : null;

    if (redirectToSignin) {
      return <Redirect to="/signin" />;
    }
    return (
      <>
        <div id="profile-container">
          <div id="profile-header">
            <div id="header-image-container">
              <img
                src={coverPhotoUrl}
                style={{
                  objectFit: "cover",
                  width: "850px"
                }}
              />
            </div>
            <div id="profile-header-bottom">
              <div id="profile-dp-un">
                <div className="circle-photo">
                  <img src={photoUrl} />
                </div>
                <h2 id="profile-username">
                  @{user ? user.username + " " : "loading"}{" "}
                  {user ? this.shitcode() : "Loading..."}
                </h2>
              </div>
              {/* <h4>{this.renderUsersName()}</h4> */}
            </div>
          </div>
          <ProfileTabs
            followers={user.followers}
            following={user.following}
            posts={posts}
            feeds={feeds}
            user={user}
          />
        </div>
        <div id="right-container" />
      </>
    );
  }
}

export default Profile;
