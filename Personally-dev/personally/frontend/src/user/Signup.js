import React, { Component } from "react";
import { signup } from "../auth";
import { Link } from "react-router-dom";
import { Progress, Segment } from "semantic-ui-react";

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: "",
      error: "",
      stepOne: true,
      stepTwo: false,
      stepThree: false,
      progress: 33
    };
  }
  handleChange = data => event => {
    this.setState({ [data]: event.target.value });
    //removes error message if they start editing something
    this.setState({ error: "" });
  };

  clickedSubmit = event => {
    event.preventDefault();

    const { firstname, lastname, username, email, password } = this.state;
    const user = {
      firstname,
      lastname,
      username,
      email,
      password,
      open: false
    };
    //send data to signup method
    signup(user).then(data => {
      //if the data contains an error, set the state
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({
          firstname: "",
          lastname: "",
          username: "",
          email: "",
          password: "",
          error: "",
          open: true
        });
      }
    });
  };

  toStepTwo = e => {
    e.preventDefault();
    this.setState({ stepOne: false });
    this.setState({ stepTwo: true });
    this.setState({ progress: 66 });
  };

  toStepThree = e => {
    e.preventDefault();
    this.setState({ stepTwo: false });
    this.setState({ stepTree: true });
    this.setState({ progress: 100 });
  };

  steppedForm = (firstname, lastname, username, email, password) => {
    if (this.state.stepOne) {
      return (
        <>
          <div className="form-group">
            <label className="text-muted">First name</label>
            <input
              onChange={this.handleChange("firstname")}
              type="text"
              className="form-control"
              value={firstname}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">last name</label>
            <input
              onChange={this.handleChange("lastname")}
              type="text"
              className="form-control"
              value={lastname}
            />
          </div>
          <button
            onClick={this.toStepTwo}
            className="btn btn-raised btn-primary"
          >
            next
          </button>
        </>
      );
    } else if (this.state.stepTwo) {
      return (
        <>
          <div className="form-group">
            <label className="text-muted">username</label>
            <input
              onChange={this.handleChange("username")}
              type="text"
              className="form-control"
              value={username}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">email</label>
            <input
              onChange={this.handleChange("email")}
              type="text"
              className="form-control"
              value={email}
            />
          </div>
          <div className="form-group">
            <button
              onClick={this.toStepThree}
              className="btn btn-raised btn-primary"
            >
              next
            </button>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="form-group">
            <label className="text-muted">password</label>
            <input
              onChange={this.handleChange("password")}
              type="password"
              className="form-control"
              value={password}
            />
          </div>

          <div className="form-group">
            <button
              onClick={this.clickedSubmit}
              className="btn btn-raised btn-primary"
            >
              submit
            </button>
          </div>
        </>
      );
    }
  };

  backButton = () => {
    if (this.state.stepTwo) {
      return (
        <button
          onClick={() =>
            this.setState({ stepOne: true, stepTwo: false, progress: 33 })
          }
        >
          back
        </button>
      );
    } else if (this.state.stepThree) {
      return (
        <button
          onClick={() =>
            this.setState({ stepTwo: true, stepThree: false, progress: 66 })
          }
        >
          back
        </button>
      );
    } else if (this.state.stepThree) {
      return (
        <button
          onClick={() =>
            this.setState({ stepTwo: true, stepThree: false, progress: 66 })
          }
        >
          back
        </button>
      );
    }
  };
  render() {
    const {
      firstname,
      lastname,
      username,
      email,
      password,
      error,
      open
    } = this.state;

    return (
      <div className="general-container">
        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>
        <form className="signup-form">
          <Segment>
            {this.backButton()}
            <h2 className="mt-5 mb-5">Create an account</h2>
            <p className="mt-5 mb-5">Start sharing in a new way.</p>
            <Progress percent={this.state.progress} attached="top" />
            {this.steppedForm(firstname, lastname, username, email, password)}
            <div
              className="alert alert-info"
              style={{ display: open ? "" : "none" }}
            >
              Account Created. Please <Link to="/signin">Sign in</Link>
            </div>
          </Segment>
        </form>
      </div>
    );
  }
}

export default Signup;
