import React, { Component } from "react";
import { list } from "./apiUser";
import { Link } from "react-router-dom";
import DefaultProfile from "../assets/images/avatar.png";

//styling
const cardStyle = {
  borderBottom: "1px solid lightgrey"
};

const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};

const pFix = {
  marginTop: "0",
  marginBottom: "0"
};

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      error: ""
    };
  }

  componentDidMount = () => {
    list().then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ users: data });
      }
    });
  };
  renderUsers = users => (
    <ul className="list-group">
      {users.map((user, i) => (
        <Link
          to={`/user/${user._id}`}
          className="list-group-item"
          style={cardStyle}
          key={i}
        >
          <div className="list-group-item-inline">
            <div className="circle-photo-list">
              <img
                style={profileImage}
                src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}`}
                onError={i => (i.target.src = `${DefaultProfile}`)}
                alt={user.name}
              />
            </div>
            <div className="user-info">
              <p className="list-group-item-heading" style={pFix}>
                {user.username}
              </p>
              <p className="list-group-item-text" style={pFix}>
                {user.firstname} {user.lastname}
              </p>
            </div>
          </div>
        </Link>
      ))}
    </ul>
  );

  render() {
    const { users } = this.state;
    return (
      <div className="general-container">
        <h2>Users</h2>
        {this.renderUsers(users)}
      </div>
    );
  }
}

export default Users;
