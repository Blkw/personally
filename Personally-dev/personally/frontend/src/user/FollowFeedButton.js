import React, { Component } from "react";
import { feedFollow, feedUnfollow } from "../feed/apiFeed";
class FollowFeedButton extends Component {
  followClick = () => {
    this.props.onButtonClick(feedFollow);
  };
  unfollowClick = () => {
    this.props.onButtonClick(feedUnfollow);
  };

  render() {
    return (
      <div className="d-inline-block mt-5">
        {!this.props.following ? (
          <button className="create-post-button" onClick={this.followClick}>
            Follow
          </button>
        ) : (
          <button className="create-post-button" onClick={this.unfollowClick}>
            unfollow
          </button>
        )}
      </div>
    );
  }
}
export default FollowFeedButton;
