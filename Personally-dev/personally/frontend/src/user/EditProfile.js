import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { read, update } from "./apiUser";
import { Redirect, Link } from "react-router-dom";
import DeleteUser from "./DeleteUser";
import imageButton from "../assets/images/imageUpload.png";

class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      firstname: "",
      lastname: "",
      email: "",
      username: "",
      password: null,
      photo: null,
      coverPhoto: null,
      redirectToProfile: false,
      error: "",
      loading: false
    };
  }

  handleChange = name => event => {
    this.setState({ error: "" });

    if (name === "photo") {
      var value = name === "photo" ? event.target.files[0] : event.target.value;
      var fileSize = name === "photo" ? event.target.files[0].size : 0;
    }
    if (name === "coverPhoto") {
      var value =
        name === "coverPhoto" ? event.target.files[0] : event.target.value;
      var fileSize = name === "coverPhoto" ? event.target.files[0].size : 0;
    }

    this.userData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  clickedSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });
    if (this.isValid()) {
      //send request to API
      const userId = this.props.match.params.userId;
      const token = isAuthenticated().token;

      update(userId, token, this.userData).then(data => {
        //   if the data contains an error, set the state
        if (data.error) this.setState({ error: data.error });
        else {
          this.setState({
            redirectToProfile: true
          });
        }
      });
    }
  };

  init = userId => {
    const token = isAuthenticated().token;
    read(userId, token).then(data => {
      if (data.error) {
        this.setState({ redirectToProfile: true });
      } else {
        this.setState({
          id: data._id,
          firstname: data.firstname,
          lastname: data.lastname,
          username: data.username,
          email: data.email
        });
      }
    });
  };

  componentDidMount() {
    this.userData = new FormData();
    const userId = this.props.match.params.userId;
    this.init(userId);
  }

  isValid = () => {
    const { firstname, lastname, username, email, password } = this.state;

    if (firstname.length === 0) {
      this.setState({ error: "Name is required." });
      return false;
    }
    if (lastname.length === 0) {
      this.setState({ error: "Last name is required." });
      return false;
    }
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      this.setState({
        error: "A valid Email is required"
      });
      return false;
    }
    if (username.length === 0) {
      this.setState({ error: "username is required." });
      return false;
    }
    if (password != null) {
      if (
        !/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          password
        )
      ) {
        this.setState({
          error:
            "Please enter a password with a minimum of eight characters, at least one letter, one number and one special character."
        });
        return false;
      }
    }

    return true;
  };

  signupForm = (firstname, lastname, username, email, password) => (
    <>
      <form>
        <div className="form-edit-profile-container">
          <div className="form-group">
            <div className="image-upload">
              <label
                htmlFor="profile-image-upload"
                className="image-upload-button"
              >
                <img
                  src={imageButton}
                  style={{
                    width: "35px",
                    marginRight: "10px",
                    cursor: "pointer"
                  }}
                />
                Profile Photo
              </label>
              <input
                onChange={this.handleChange("photo")}
                type="file"
                accept="image/*"
                className="form-control"
                id="profile-image-upload"
              />
            </div>

            <div className="image-upload">
              <label
                htmlFor="cover-image-upload"
                className="image-upload-button"
              >
                <img
                  src={imageButton}
                  style={{
                    width: "35px",
                    marginRight: "10px",
                    cursor: "pointer"
                  }}
                />
                Cover Photo
              </label>
              <input
                onChange={this.handleChange("coverPhoto")}
                type="file"
                accept="image/*"
                className="form-control"
                id="cover-image-upload"
              />
            </div>
          </div>
          <div className="form-group">
            <label className="text-muted">first name</label>
            <input
              onChange={this.handleChange("firstname")}
              type="text"
              className="form-control"
              value={firstname}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">last name</label>
            <input
              onChange={this.handleChange("lastname")}
              type="text"
              className="form-control"
              value={lastname}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">username</label>
            <input
              onChange={this.handleChange("username")}
              type="text"
              className="form-control"
              value={username}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">email</label>
            <input
              onChange={this.handleChange("email")}
              type="text"
              className="form-control"
              value={email}
            />
          </div>
          <div className="form-group">
            <label className="text-muted">password</label>
            <input
              onChange={this.handleChange("password")}
              type="password"
              className="form-control"
              value={password}
            />
          </div>

          <button onClick={this.clickedSubmit} className="btn-primary">
            update
          </button>
        </div>
      </form>
      {/* <Link
        className="btn btn-raised btn-success mr-5"
        to={`/user/edit/${this.state.user._id}`}
      >
        Edit Profile
      </Link>
      <DeleteUser userId={this.state.user._id} /> */}
    </>
  );

  render() {
    const {
      id,
      firstname,
      lastname,
      username,
      email,
      password,
      redirectToProfile,
      error,
      loading
    } = this.state;

    if (redirectToProfile) {
      return <Redirect to={`/user/${id}`} />;
    }

    const photoUrl = id
      ? `${
          process.env.REACT_APP_API_URL
        }/user/photo/${id}?${new Date().getTime()}`
      : "";

    return (
      <div id="profile-container">
        <h2>Edit profile</h2>
        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>
        {isAuthenticated().user.role === "admin" ||
          (isAuthenticated().user._id === id &&
            this.signupForm(firstname, lastname, username, email, password))}
        {loading ? <div className="loading">Loading...</div> : ""}
      </div>
    );
  }
}
export default EditProfile;
