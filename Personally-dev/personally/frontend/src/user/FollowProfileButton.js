import React, { Component } from "react";
import { follow, unfollow } from "./apiUser";
import { feedFollow, feedUnfollow } from "../feed/apiFeed";

import { isAuthenticated } from "../auth";

class FollowProfileButton extends Component {
  followUserClick = () => {
    const newsfeedId = this.props.newsfeedId;
    const token = isAuthenticated().token;
    const user = this.props.user;

    follow(newsfeedId, token, user).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        console.log("Followed");
      }
    });
  };

  unfollowUserClick = () => {
    const newsfeedId = this.props.newsfeedId;
    const user = this.props.user;

    const token = isAuthenticated().token;

    unfollow(newsfeedId, token, user).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        console.log("unfollowed");
      }
    });
  };

  followFeedClick = () => {
    const newsfeedId = this.props.newsfeedId;
    const token = isAuthenticated().token;
    const feed = this.props.feed;
    feedFollow(newsfeedId, token, feed).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        console.log("Followed");
      }
    });
  };

  unfollowFeedClick = () => {
    const newsfeedId = this.props.newsfeedId;
    const token = isAuthenticated().token;
    const feed = this.props.feed;
    feedUnfollow(newsfeedId, token, feed).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        console.log("unfollowed");
      }
    });
  };

  render() {
    if (this.props.followType == "user") {
      return (
        <div className="d-inline-block mt-5">
          {!this.props.following ? (
            <button id="follow-button" onClick={this.followUserClick}>
              Follow
            </button>
          ) : (
            <button id="unfollow-button" onClick={this.unfollowUserClick}>
              unfollow
            </button>
          )}
        </div>
      );
    } else if (this.props.followType == "feed") {
      return (
        <div className="d-inline-block mt-5">
          {!this.props.following ? (
            <button id="follow-button" onClick={this.followFeedClick}>
              Follow
            </button>
          ) : (
            <button id="unfollow-button" onClick={this.unfollowFeedClick}>
              unfollow
            </button>
          )}
        </div>
      );
    }
  }
}
export default FollowProfileButton;
