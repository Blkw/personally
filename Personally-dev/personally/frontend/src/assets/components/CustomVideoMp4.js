import React, { Component } from "react";
import "../../../node_modules/video-react/dist/video-react.css"; // import css
import {
  Player,
  ControlBar,
  CurrentTimeDisplay,
  TimeDivider,
  VolumeMenuButton,
  BigPlayButton,
  PosterImage
} from "video-react";

export default class CustomVideoMp4 extends Component {
  render = () => {
    const { thisLink } = this.props;

    return (
      <Player PosterImage="/assets/poster.png">
        <source src={`${thisLink}`} />
        <ControlBar>
          <CurrentTimeDisplay order={4.1} />
          <TimeDivider order={4.2} />
          <VolumeMenuButton />
          <BigPlayButton position="center" />
        </ControlBar>
      </Player>
    );
  };
}
