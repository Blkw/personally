import React, { Component } from "react";
import { isAuthenticated } from "../../auth";
import { follow, unfollow, newsfeedsByUser } from "../../newsfeed/apiNewsfeed";
import { Header, Modal } from "semantic-ui-react";
import FollowFeedButton from "../../user/FollowFeedButton";
import FollowProfileButton from "../../user/FollowProfileButton";

export default class ChooseFollow extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      user: {},
      loading: false,
      tags: "",
      newsfeeds: [],
      selectedNewsFeed: "",
      feed: {},
      following: false
    };
  }

  getUsersNewsfeeds = userId => {
    const token = isAuthenticated().token;
    newsfeedsByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ newsfeeds: data });
      }
    });
  };

  componentDidMount() {
    this.getUsersNewsfeeds(isAuthenticated().user._id);
    this.postData = new FormData();
    this.setState({ user: isAuthenticated().user });
  }

  handleChange = name => event => {
    this.setState({ error: "" });
    const value = name === "photo" ? event.target.files[0] : event.target.value;
    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.postData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  preventDefault = event => {
    event.preventDefault();
  };

  renderFollowButton = id => {
    let feed = this.props.selectedfeed._id;
    let newsfeeds = this.state.newsfeeds;

    let isFollowing = false;

    for (let i = 0; i < newsfeeds.length; i++) {
      if (newsfeeds[i]._id == id) {
        console.log(newsfeeds);

        let following = newsfeeds[i].following;
        for (let x = 0; x < following.length; x++) {
          console.log(following[x]._id, "==", feed);
          if (newsfeeds[i].following[x]._id == feed) {
            isFollowing = true;
          }
        }
      }
    }
    return (
      <FollowProfileButton
        type="button"
        followType="feed"
        following={isFollowing}
        newsfeedId={id}
        feed={feed}
        onClick={this.preventDefault}
      />
    );
  };
  renderNewsfeedsList = () => {
    const { newsfeeds } = this.props;

    return (
      <>
        <ul className="follow-actions-list">
          {newsfeeds.map((newsfeed, i) => {
            return (
              <li
                className="follow-actions-list-item"
                key={i}
                value={`${newsfeed._id}`}
              >
                <p className="follow-actions-newfeed-name">{newsfeed.title}</p>
                {this.renderFollowButton(newsfeed._id)}
              </li>
            );
          })}
        </ul>
      </>
    );
  };

  getFollowButton = () => {
    let hasContent = this.state.selectedNewsFeed;
    const newsfeeds = this.props.newsfeeds;

    return (
      <Modal
        on="click"
        trigger={<button id="follow-button">Follow settings</button>}
        centered={false}
      >
        <Modal.Header className="createFeed">Create a feed</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Header>
              Which newsfeed would you like "{this.props.selectedfeed.title}" to
              show in?
            </Header>
            <form onSubmit={this.onSubmit}>{this.renderNewsfeedsList()}</form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  };

  onSubmit(event) {
    event.preventDefault();
  }

  render() {
    const newsfeeds = this.props.newsfeeds;

    if (
      newsfeeds.some(newsfeed => (newsfeed.following = this.props.selectedfeed))
    ) {
      return (
        <>
          <p>This feeds posts can be seen on one or more of you newsfeeds</p>
          {this.getFollowButton()}
        </>
      );
    } else {
      this.getFollowButton();
    }
  }
}
