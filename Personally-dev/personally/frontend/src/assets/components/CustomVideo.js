import React, { Component } from "react";
import ReactPlayer from "react-player";

export default class CustomVideo extends Component {
  render = () => {
    const { thisLink } = this.props;
    return (
      <ReactPlayer className="embeded-video" light controls url={thisLink} />
    );
  };
}
