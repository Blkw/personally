import React, { Component } from "react";
import { Header, Modal } from "semantic-ui-react";
import { isAuthenticated } from "../../auth";
import { create } from "../../feed/apiFeed";

export default class CreateFeed extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      user: {},
      loading: false,
      tags: ""
    };
  }

  componentDidMount() {
    this.postData = new FormData();
    this.setState({ user: isAuthenticated().user });
  }

  handleChange = name => event => {
    this.setState({ error: "" });
    const value = name === "photo" ? event.target.files[0] : event.target.value;
    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.postData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  clickedSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });

    //send request to API
    const userId = this.state.user._id;
    const token = isAuthenticated().token;

    console.log("clicked submit   ", userId);
    create(userId, token, this.postData).then(data => {
      //   if the data contains an error, set the state
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({
          loading: false,
          title: "",
          tags: ""
        });
        window.location.reload();
      }
    });
  };

  render() {
    let hasContent = this.state.title.length > 2;

    return (
      <Modal
        on="click"
        trigger={<li className="create-feed-button">+</li>}
        centered={false}
      >
        <Modal.Header className="createFeed">Create a feed</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Header>Ready to make some magic?</Header>
            <form>
              <div class="ui input">
                <input
                  type="text"
                  placeholder="Feed name"
                  onChange={this.handleChange("title")}
                />
              </div>
              <div class="ui input">
                <input
                  type="text"
                  placeholder="#"
                  onChange={this.handleChange("tags")}
                />
              </div>
            </form>
          </Modal.Description>
        </Modal.Content>
        {hasContent ? (
          <button className="create-post-button" onClick={this.clickedSubmit}>
            Create Feed
          </button>
        ) : null}
      </Modal>
    );
  }
}
