import React, { Component } from "react";
import { Button, Modal } from "semantic-ui-react";
import { remove, uncomment } from "../../post/apiPost";
import { isAuthenticated } from "../../auth";

class DeleteMessage extends Component {
  state = { open: true };

  show = size => () => this.setState({ size, open: true });
  close = () => this.setState({ open: false });

  deletePost = () => {
    console.log("deletePost called");

    const { post } = this.props;
    const postId = post._id;
    const token = isAuthenticated().token;
    remove(postId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        window.location.reload();
      }
    });
  };

  deleteComment = comment => {
    console.log("deleteComment called");
    const token = isAuthenticated().token;
    const userId = isAuthenticated().user._id;

    uncomment(userId, token, comment).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.props.updatedComments(data.comments);
      }
    });
  };

  render() {
    const { open, size } = this.state;
    const { type } = this.props;
    const { comment } = this.props;

    console.log("delete message called");

    if (type == "post") {
      return (
        <div>
          <Modal size={size} open={open} onClose={this.close}>
            <Modal.Header>{this.props.title}</Modal.Header>
            <Modal.Content>
              <p>{this.props.message}</p>
            </Modal.Content>
            <Modal.Actions>
              <Button negative>No</Button>
              <Button
                positive
                icon="checkmark"
                labelPosition="right"
                content="Yes"
                //this doesnt work for some reason:
                onClick={() => {
                  this.deletePost();
                }}
              />
            </Modal.Actions>
          </Modal>
        </div>
      );
    } else if (type == "comment") {
      return (
        <div>
          <Modal size={size} open={open} onClose={this.close}>
            <Modal.Header>{this.props.title}</Modal.Header>
            <Modal.Content>
              <p>{this.props.message}</p>
            </Modal.Content>
            <Modal.Actions>
              <Button negative>No</Button>
              <button
                //this doesnt work for some reason:
                onClick={() => {
                  this.deleteComment(comment);
                }}
              >
                yes
              </button>
            </Modal.Actions>
          </Modal>
        </div>
      );
    }
  }
}

export default DeleteMessage;
