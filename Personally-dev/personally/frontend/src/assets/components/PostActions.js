import React, { Component } from "react";
import {
  singlePost,
  like,
  unlike,
  singlePostComments
} from "../../post/apiPost";
import { isAuthenticated } from "../../auth";
import { Redirect, Link } from "react-router-dom";
import Comment from "../../post/Comment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faHeart as solidHeart } from "@fortawesome/free-solid-svg-icons";
import { faHeart as lineHeart } from "@fortawesome/free-regular-svg-icons";
import { faComment as comment } from "@fortawesome/free-regular-svg-icons";
import { faClone as clone } from "@fortawesome/free-regular-svg-icons";

// import { faHeart } from "@fortawesome/fontawesome-svg-core";

class PostActions extends Component {
  state = {
    post: "",
    redirectToHome: false,
    redirectToSignin: false,
    like: false,
    likes: 0,
    comments: [],
    showComments: false,
    noComments: 1,
    anyComments: true,
    notes: 0
  };

  updateComments = comments => {
    this.setState({ comments });
  };

  checkLike = likes => {
    const userId = isAuthenticated() && isAuthenticated().user._id;
    let match = likes.indexOf(userId) !== -1;
    return match;
  };

  componentDidMount = () => {
    const { post } = this.props;
    const postId = post._id;
    singlePost(postId).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({
          post: data,
          likes: data.likes.length,
          like: this.checkLike(data.likes),
          comments: data.comments,
          notes: data.notes
        });
      }
    });
  };

  notesCounter = () => {
    const notes = this.state.notes;
    return <p className="notes">{notes} notes</p>;
  };

  likeToggle = () => {
    if (!isAuthenticated()) {
      this.setState({ redirectToSignin: true });
      return false;
    }
    let callApi = this.state.like ? unlike : like;
    const userId = isAuthenticated().user._id;
    const postId = this.state.post._id;
    const token = isAuthenticated().token;

    callApi(userId, token, postId).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({
          like: !this.state.like,
          likes: data.likes.length
        });
      }
    });
  };

  toggleComments = () => {
    if (!this.state.showComments) {
      this.setState({ showComments: true });
    } else {
      this.setState({ showComments: false });
    }
  };

  loadMore = (postId, number) => {
    this.setState({ noComments: this.state.comments.length + number });
    this.pushComments(postId, this.state.noComments + number);
  };

  loadMoreCheck = (postId, comments) => {
    if (this.state.anyComments && this.state.comments.length > 4) {
      return (
        <button
          className="load-comments-button"
          onClick={() => this.loadMore(postId, comments)}
        >
          Load more comments
        </button>
      );
    } else if (this.state.anyComments && this.state.comments.length < 4) {
    } else {
      return <p className="no-more-comments"> no more comments</p>;
    }
  };

  pushComments = (postId, noComments) => {
    console.log(postId, " ", noComments);
    singlePostComments(postId, noComments).then(data => {
      console.log(data.error);

      if (data.error) {
        console.log(data.error);
      } else {
        console.log(data);

        if (!(data.length <= 0)) {
          var merged = [...this.state.comments, ...data];
          this.setState({
            comments: merged
          });
        } else {
          this.setState({ anyComments: false });
          console.log("no more posts left to load :(");
        }
      }
    });
  };

  updateNotes = notes => {
    this.setState({ notes: notes });
  };

  renderComments = () => {
    const { post } = this.props;
    const { comments } = this.state;

    if (this.state.showComments) {
      return (
        <>
          <Comment
            postId={post._id}
            comments={comments}
            updateComments={this.updateComments}
            updateNotes={this.updateNotes}
            notes={this.state.notes}
          />
          {this.loadMoreCheck(post._id, comments.length)}
        </>
      );
    }
    return false;
  };

  render() {
    const { post } = this.props;
    const { like, likes, comments } = this.state;
    const { redirectToHome, redirectToSignin } = this.state;

    if (redirectToHome) {
      return <Redirect to={`/`} />;
    } else if (redirectToSignin) {
      return <Redirect to={`/signin`} />;
    }
    return (
      <div>
        <div className="post-actions">
          {like ? (
            <div className="actions-tab" onClick={this.likeToggle}>
              <p>
                <FontAwesomeIcon icon={solidHeart} color="red" />
              </p>
            </div>
          ) : (
            <div className="actions-tab" onClick={this.likeToggle}>
              <p>
                <FontAwesomeIcon icon={lineHeart} />
              </p>
            </div>
          )}
          <div
            className="actions-tab"
            onClick={() => {
              this.toggleComments();
            }}
          >
            <FontAwesomeIcon icon={comment} />
            <p>Comments</p>
          </div>
          <div className="actions-tab">
            <FontAwesomeIcon icon={clone} />
            <p>Repost</p>
          </div>
        </div>
        {this.notesCounter()}

        {this.renderComments()}
      </div>
    );
  }
}

export default PostActions;
