import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { isAuthenticated } from "../../auth/";
import { Link } from "react-router-dom";
import DefaultProfile from "../images/avatar.png";
import PostActionsPopup from "./PostActionsPopup";
import PostActions from "./PostActions";
import Linkify from "linkifyjs/react";
import ReactTinyLink from "react-tiny-link";
import CustomVideo from "./CustomVideo";
import CustomVideoMp4 from "./CustomVideoMp4";
import CreateNewsfeed from "./CreateNewsfeed";

import {
  read,
  newsfeedsByUser,
  postsByNewsfeed
} from "../../newsfeed/apiNewsfeed";
import Post from "../../post/Posts";

//styling
const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};
const postImage = {
  width: "100%",
  height: "auto",
  borderRadius: "10px",
  margin: "0"
};

const text = {
  marginTop: "0",
  marginBottom: "0"
};

const tags = {
  marginTop: "15px",
  marginBottom: "0",
  color: "#D2D7DC"
};

const endOfFeed = {
  textAlign: "center",
  width: "550px",
  color: "lightGrey"
};

class ProfileTabs extends Component {
  constructor() {
    super();
    this.state = {
      redirectToSignin: false,
      feedsPosts: [],
      selectedNewsfeed: null,
      newsfeeds: [],
      following: false,
      page: 1,
      anyPosts: true
    };
  }

  componentDidMount() {
    this.getUsersNewsfeeds(isAuthenticated().user._id, this.state.page);
  }

  loadMore = (feedId, number) => {
    this.setState({ page: this.state.page.length + number });
    this.pushFeed(feedId, this.state.page + number);
  };

  loadLess = (feedId, number) => {
    this.setState({ page: this.state.page - number });
    this.loadFeed(feedId, this.state.page - number);
  };

  getUsersNewsfeeds = userId => {
    const token = isAuthenticated().token;
    newsfeedsByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ newsfeeds: data });
        console.log(data);
      }
    });
  };

  init = feedId => {
    const token = isAuthenticated().token;
    read(feedId, token).then(data => {
      if (data.error) {
        this.setState({ redirectToSignin: true });
        console.log("Error.");
      } else {
        this.setState({ newsfeed: data });
      }
    });
  };

  seeMore = post => {
    if (post.text.length > 400) {
      return (
        <Link className="comments-error" to={`/post/${post._id}`}>
          <br />
          <br />
          See more...
        </Link>
      );
    }
  };

  renderLinkPreview = text => {
    var matches = text.match(/(((https?:\/\/)|(www\.))[^\s]+)/g);

    if (matches != null) {
      var thisLink = matches[0];

      if (thisLink.search(/^http[s]?\:\/\//) === -1) {
        thisLink = "http://" + thisLink;
      }

      var hasyoutube = thisLink.includes("youtube");
      var hasVimeo = thisLink.includes("vimeo");
      var notFriendly = thisLink.includes(".mp4");

      if (hasyoutube || hasVimeo) {
        return <CustomVideo thisLink={thisLink} />;
      } else if (notFriendly) {
        return <CustomVideoMp4 thisLink={thisLink} />;
      } else {
        return (
          <ReactTinyLink
            cardSize="large"
            showGraphic={true}
            maxLine={3}
            minLine={1}
            url={`${thisLink}`}
          />
        );
      }
    } else return false;
  };

  loadFeed = (feedId, page) => {
    this.init(feedId);
    this.setState({ feedsPosts: [] });
    this.setState({ anyPosts: true });

    const token = isAuthenticated().token;
    postsByNewsfeed(feedId, page, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ selectedFeed: feedId });
        console.log("After setting state: ", this.state.selectedFeed);
        console.log("posts in loadFeed: \n\n", data, " \n\n");
        this.setState({ feedsPosts: data });
      }
    });
  };

  pushFeed = (feedId, page) => {
    this.init(feedId);
    const token = isAuthenticated().token;
    postsByNewsfeed(feedId, page, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ selectedFeed: feedId });
        console.log(data);

        if (!(data.length <= 0)) {
          var merged = [...this.state.feedsPosts, ...data];
          this.setState({
            feedsPosts: merged
          });
        } else {
          this.setState({ anyPosts: false });
          console.log("no more posts left to load :(");
        }
      }
    });
  };

  allPosts = () => {
    const { posts } = this.props;
    return (
      <>
        <Post />
      </>
    );
  };

  newsfeedsPosts = newsfeedId => {
    var posts = this.state.feedsPosts;

    var page = this.state.page;
    return (
      <>
        {posts.map((post, i) => {
          const posterId = post.postedBy ? post.postedBy._id : "";
          const posterName = post.postedBy ? post.postedBy.username : "Unknown";
          return (
            <div key={i} className="feed-post">
              <div className="author-section">
                <Link to={`/user/${posterId}`}>
                  <div className="circle-photo-list">
                    <img
                      style={profileImage}
                      src={`${process.env.REACT_APP_API_URL}/user/photo/${posterId}`}
                      onError={i => (i.target.src = `${DefaultProfile}`)}
                      alt={posterName}
                    />
                  </div>
                </Link>
                <Link to={`/user/${posterId}`}>
                  <p className="poster">{posterName}</p>
                </Link>
                {/* {this.dateTime(post.created)} */}
                <PostActionsPopup post={post} />
              </div>
              <div className="post-content">
                <Link to={`/post/${post._id}`}>
                  <img
                    alt={post.text}
                    style={postImage}
                    src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
                    onError={i => (i.target.style.display = "none")}
                  />
                </Link>
                <p className="list-group-item-heading" style={text}>
                  <Linkify>{post.text.substring(0, 400)}</Linkify>
                  {/*  makes the maxiumum text on the feed 400 chars */}
                </p>
                {this.renderLinkPreview(post.text)}
                {this.seeMore(post)}
                <p className="list-group-item-text" style={tags}>
                  #{post.tags}
                </p>
              </div>
              <PostActions post={post} />
            </div>
          );
        })}
        {this.loadMoreCheck(newsfeedId, posts.length)}
      </>
    );
  };

  loadMoreCheck = (newsfeedId, posts) => {
    if (this.state.anyPosts) {
      return (
        <button
          className="load-more-button"
          onClick={() => this.loadMore(newsfeedId, posts)}
        >
          Load more
        </button>
      );
    } else {
      return <h5 style={endOfFeed}> ----- You're all caught up! ----- </h5>;
    }
  };

  shitcode = () => {
    if (isAuthenticated() && this.state.newsfeeds.length < 5) {
      return <CreateNewsfeed />;
    } else {
      return null;
    }
  };

  render() {
    //will need to loop through all tabs
    //create tab popup

    const { newsfeeds } = this.state;

    return (
      <div>
        <Tabs>
          <TabList id="newsfeed-nav">
            <Tab className="feed-nav-item">All posts</Tab>
            {newsfeeds.map((newsfeed, i) => {
              return (
                <Tab
                  onClick={() => {
                    this.loadFeed(newsfeed._id, 1);
                  }}
                  className="feed-nav-item"
                  key={i}
                >
                  {newsfeed.title}
                </Tab>
              );
            })}
            {newsfeeds ? this.shitcode() : "Loading..."}
          </TabList>
          <div className="feedTabs">
            <TabPanel>{this.allPosts()}</TabPanel>
            {newsfeeds.map((newsfeed, i) => {
              return (
                <TabPanel key={i}>
                  <div
                    className="general-container general-container-newsfeed"
                    onScroll={this.handleScroll}
                  >
                    {this.newsfeedsPosts(newsfeed._id)}
                  </div>
                </TabPanel>
              );
            })}
          </div>
        </Tabs>
      </div>
    );
  }
}
export default ProfileTabs;
