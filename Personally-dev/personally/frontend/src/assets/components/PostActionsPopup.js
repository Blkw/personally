import React, { Component } from "react";
import { Button, Popup, Grid } from "semantic-ui-react";
import { remove } from "../../post/apiPost";
import { isAuthenticated } from "../../auth";
import DeleteMessage from "./DeleteMessage";
import { Redirect } from "react-router-dom";

const popupStyle = {
  backgroundColor: "white",
  padding: "0px 5px 0px 5px",
  boxShadow: "0px 0px 10px -5px lightgrey",
  border: "solid 1px #D2D7DC",
  borderRadius: "10px 10px 10px 0px"
};
const gridRow = {
  borderBottom: "solid 1px #D2D7DC",
  paddingBottom: "3px",
  marginBottom: "-15px",
  width: "100px",
  cursor: "pointer"
};
const gridRowLast = {
  paddingBottom: "3px",
  paddingTop: "0px",
  cursor: "pointer"
};

const triDotButton = {
  backgroundColor: "transparent",
  border: "none",
  color: "#52606F",
  fontWeight: "bold",
  cursor: "pointer",
  outlineWidth: "0"
};

class PostActionsPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showComponent: false,
      redirectToProfile: false,
      redirectToUpdate: false
    };
  }

  sharePost = () => {
    const { post } = this.props;
    const postId = post._id;
    const shareURL = `https://www.personally.com/post/${postId}/`;
    console.log("copied share URL for post", shareURL);
    //copy to clip board
  };

  // deletePost = () => {
  //   const { post } = this.props;
  //   const id = post._id;
  //   const token = isAuthenticated().token;
  //   remove(id, token).then(data => {
  //     if (data.error) {
  //       console.log(data.error);
  //     } else {
  //       // this.setState({ redirectToProfile: true });
  //       window.location.reload();
  //     }
  //   });
  // };

  deletePrompt = () => {
    this.setState({
      showComponent: true
    });
    console.log("DeletePrompt called");
  };

  goToUpdate = () => {
    this.setState({
      redirectToUpdate: true
    });
  };
  renderRedirect = () => {
    const { post } = this.props;
    const id = post._id;
    if (this.state.redirectToUpdate) {
      return <Redirect to={`/post/edit/${id}`} />;
    }
  };

  // const { post } = this.props;
  //   const id = post._id;
  //   console.log("go to update ", id);
  //   return <Redirect to={`/post/edit/${id}`} />;

  PostActionsPopupPoster = () => {
    const { post } = this.props;

    return (
      <>
        <Popup
          trigger={<Button style={triDotButton}>...</Button>}
          flowing
          on="click"
        >
          <Grid centered divided columns={1} style={popupStyle}>
            <Grid.Column textAlign="left">
              <Grid.Row>
                <p style={gridRow} onClick={this.sharePost}>
                  share
                </p>
              </Grid.Row>
              <Grid.Row>
                {this.renderRedirect()}
                <p style={gridRow} onClick={this.goToUpdate}>
                  edit
                </p>
              </Grid.Row>
              <Grid.Row>
                <p style={gridRowLast} onClick={this.deletePrompt}>
                  delete
                </p>
                {this.state.showComponent ? (
                  <DeleteMessage
                    title={"Delete post?"}
                    message={"Are you sure you want to delete this post?"}
                    post={post}
                  />
                ) : null}
              </Grid.Row>
            </Grid.Column>
          </Grid>
        </Popup>
      </>
    );
  };

  PostActionsPopupNotPoster = () => {
    return (
      <>
        <Popup
          trigger={<Button style={triDotButton}>...</Button>}
          flowing
          on="click"
        >
          <Grid centered divided columns={1} style={popupStyle}>
            <Grid.Column textAlign="left">
              <Grid.Row>
                <p style={gridRowLast}>share</p>
              </Grid.Row>
            </Grid.Column>
          </Grid>
        </Popup>
      </>
    );
  };

  render() {
    const { post } = this.props;
    const { redirectToProfile } = this.state;

    if (redirectToProfile) {
      window.location.reload();
    }

    return (
      <div className="post-interact-options">
        {isAuthenticated().user &&
          isAuthenticated().user._id != post.postedBy._id && (
            <>{this.PostActionsPopupNotPoster()}</>
          )}
        {isAuthenticated().user &&
          isAuthenticated().user._id === post.postedBy._id && (
            <> {this.PostActionsPopupPoster()}</>
          )}
      </div>
    );
  }
}

export default PostActionsPopup;
