import React from "react";
import { Link, withRouter } from "react-router-dom";
import { isAuthenticated, signout } from "../auth";
import logo from "../assets/images/inifeed-color.png";

const isActive = (history, path) => {
  if (history.location.pathname === path)
    return {
      color: "#52606F",
      fontWeight: "bold",
      borderLeft: "solid 2px #52606F"
    };
  else return { color: "#919AA3" };
};

const Menu = ({ history }) => (
  <nav className="navbar">
    <div className="navbar-header">
      <div className="top-section1-nav">
        <Link className="navbar-brand" to="/">
          <img src={`${logo}`} className="logo"/>
        </Link>
      </div>
      <div className="top-section2-nav">
        <input
          type="text"
          placeholder="search ( doesn't work )"
          id="header-search"
        />
      </div>
      <div className="top-section3-nav">
        <li className="nav-item-top">
          {isAuthenticated() && (
            <Link
              className="nav-link"
              style={isActive(history, `/user/${isAuthenticated().user._id}`)}
              to={`/user/${isAuthenticated().user._id}`}
            >
              <i class="material-icons">person</i>
              <p>{`${isAuthenticated().user.username}`}</p>
            </Link>
          )}
        </li>
      </div>
    </div>

    <div className="container-fluid">
      <ul className="nav navbar-nav">
        <p style={{ color: "lightgrey" }}> MENU </p>
        <li className="nav-item">
          <Link className="nav-link" style={isActive(history, "/")} to="/">
            <i class="material-icons">home</i>
            <p>Home</p>
          </Link>
        </li>
        <li className="nav-item">
          <i class="material-icons">explore</i>
          <p>Explore (coming soon)</p>
        </li>
        {isAuthenticated() && (
          <li className="nav-item">
            <Link
              className="nav-link"
              style={isActive(history, `/user/${isAuthenticated().user._id}`)}
              to={`/user/${isAuthenticated().user._id}`}
            >
              <i class="material-icons">persons</i>
              <p>My Profile</p>
            </Link>
          </li>
        )}

        <li className="nav-item">
          <Link
            className="nav-link"
            style={isActive(history, "/users")}
            to="/users"
          >
            <p>Users</p>
          </Link>
        </li>
        <li className="nav-item">
          <i class="material-icons">people</i>
          <p>Groups (coming soon)</p>
        </li>
      </ul>
      <ul className="nav navbar-nav">
        {!isAuthenticated() && (
          <>
            <li className="nav-item">
              <Link
                className="nav-link"
                style={isActive(history, "/signup")}
                to="/signup"
              >
                <span className="glyphicon glyphicon-user" /> Sign Up
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className="nav-link"
                style={isActive(history, "/signin")}
                to="/signin"
              >
                <span className="glyphicon glyphicon-log-in" /> Login
              </Link>
            </li>
          </>
        )}
        {isAuthenticated() && (
          <>
            <li className="nav-item">
              <Link
                className="nav-link"
                style={isActive(
                  history,
                  `/user/edit/${isAuthenticated().user._id}`
                )}
                to={`/user/edit/${isAuthenticated().user._id}`}
              >
                <i class="material-icons">settings</i>
                <p>Settings</p>
              </Link>
            </li>

            <li className="nav-item">
              <p
                className="nav-link"
                onClick={() => signout(() => history.push("/"))}
              >
                <i class="material-icons">cancel</i>
                signout
              </p>
            </li>
          </>
        )}
      </ul>
    </div>
  </nav>
);

export default withRouter(Menu);
