import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { create } from "../post/apiPost";
import DefaultProfile from "../assets/images/avatar.png";
import imageButton from "../assets/images/imageUpload.png";
import FeedHandler from "../assets/components/FeedHandler";
import TextareaAutosize from "react-textarea-autosize";
import { feedsByUser } from "../user/apiUser";
import { WithContext as ReactTags } from "react-tag-input";
//also npm installed react-dnd,  react-dnd-html5-backend

const profileImage = {
  width: "45px",
  gridRow: "2",
  gridColumn: "1"
};

const imageUploadStyle = {};

const KeyCodes = {
  comma: 188,
  enter: 13
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

class Home extends Component {
  constructor() {
    super();
    this.state = {
      text: "",
      tags: [],
      photo: "",
      error: "",
      user: {},
      fileSize: 0,
      loading: false,
      feeds: [],
      selectedFeed: "",
      posts: [],
      previewImage: ""
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAddition = this.handleAddition.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
  }
  handleDelete(i) {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i)
    });
  }

  handleAddition(tag) {
    this.setState(state => ({ tags: [...state.tags, tag] }));
  }

  handleDrag(tag, currPos, newPos) {
    const tags = [...this.state.tags];
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    this.setState({ tags: newTags });
  }

  handleChange = name => event => {
    this.setState({ error: "" });
    const value = name === "photo" ? event.target.files[0] : event.target.value;
    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.postData.set(name, value);

    if (name === "photo") {
      this.setState({
        previewImage: URL.createObjectURL(event.target.files[0])
      });
    }
    if (name === "selectedFeed") {
      console.log(name, value);
    }
    this.setState({ [name]: value, fileSize });

    console.log(this.state.tags);
  };

  clickedSubmit = event => {
    const { posts } = this.state;
    event.preventDefault();
    this.setState({ loading: true });

    if (this.isValid()) {
      //send request to API
      const userId = isAuthenticated().user._id;
      const feedId = this.state.selectedFeed;
      console.log("feedId in clicked submit: ", feedId);

      const token = isAuthenticated().token;
      var tagsArray = [];

      for (let i = 0; i < this.state.tags.length; i++) {
        tagsArray.splice(i, 0, this.state.tags[i].text);
      }

      this.postData.append("tags", tagsArray);

      create(userId, feedId, token, this.postData).then(data => {
        //   if the data contains an error, set the state
        if (data.error) {
          this.setState({ error: data.error });
        } else {
          console.log("new post : ", data);
          this.setState({
            loading: false,
            text: "",
            tags: [],
            photo: ""
          });
          window.location.reload();
        }
      });
    }
  };

  componentDidMount() {
    this.postData = new FormData();
    this.setState({ user: isAuthenticated().user });
    this.getUsersFeeds(isAuthenticated().user._id);
  }

  isValid = () => {
    const { text, selectedFeed } = this.state;

    if (text.length < 5) {
      this.setState({
        error: "To make a post you need to use more than 5 characters.",
        loading: false
      });
      return false;
    }
    if (!selectedFeed) {
      this.setState({
        error: "To make a post you need to choose a feed for it to belong to.",
        loading: false
      });
      return false;
    }

    return true;
  };

  getUsersFeeds = userId => {
    const token = isAuthenticated().token;
    feedsByUser(userId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ feeds: data });
      }
    });
  };

  setFeed = id => {
    return console.log("SELECTED FEED: ", id);
    this.setState.selectedFeed = id;
  };

  renderFeedList = () => {
    const { feeds } = this.state;

    return (
      <>
        <select
          className="create-post-feed-select"
          onChange={this.handleChange("selectedFeed")}
        >
          <option value="" selected disabled hidden>
            Choose a feed
          </option>
          {feeds.map((feed, i) => {
            return (
              <option
                key={i}
                value={`${feed._id}`}
                onClick={() => {
                  this.setFeed(feed._id);
                }}
              >
                {feed.title}
              </option>
            );
          })}
        </select>
      </>
    );
  };

  clearPreviewImage = () => {
    this.setState({ previewImage: "", photo: "", fileSize: 0 });
  };

  renderPreviewImage = () => {
    if (this.state.previewImage) {
      return (
        <>
          <img className="preview-image-upload" src={this.state.previewImage} />
          <button
            className="clear-preview-image"
            onClick={() => {
              this.clearPreviewImage();
            }}
          >
            x
          </button>
        </>
      );
    } else {
      return null;
    }
  };

  shitcode = () => {
    if (isAuthenticated()) {
      let hasContent = this.state.text.length > 3;
      const feeds = this.state.feeds;
      const { tags, suggestions } = this.state;

      return (
        <form
          onSubmit={e => {
            e.preventDefault();
          }}
        >
          <div className="create-post-header">
            <div className="create-post-heading">Create a post</div>
            {this.renderFeedList()}
          </div>
          <div className="create-post-main">
            <div className="circle-photo-list-create">
              <img
                style={profileImage}
                src={`${process.env.REACT_APP_API_URL}/user/photo/${
                  isAuthenticated().user._id
                }`}
                onError={i => (i.target.src = `${DefaultProfile}`)}
                alt={isAuthenticated().user.username}
              />
            </div>
            <TextareaAutosize
              className="create-post-content text-area"
              placeholder="What would you like to share?"
              name="text"
              onChange={this.handleChange("text")}
            />
          </div>
          <div className="border-bottom" />
          <div className="bottom-create-post">
            {/* <input
              type="text"
              className="post-tags"
              placeholder="#tags"
              onChange={this.handleChange("tags")}
            /> */}
            <ReactTags
              tags={tags}
              handleDelete={this.handleDelete}
              handleAddition={this.handleAddition}
              handleDrag={this.handleDrag}
              delimiters={delimiters}
              onChange={this.handleChange("tags")}
            />
            <div className="image-upload">
              <label htmlFor="image-upload" className="image-upload-button">
                <img
                  src={imageButton}
                  style={{
                    width: "35px",
                    marginRight: "10px",
                    cursor: "pointer"
                  }}
                />
              </label>
              <input
                onChange={this.handleChange("photo")}
                type="file"
                accept="image/*"
                className="form-control"
                id="image-upload"
              />
            </div>
          </div>
          {this.renderPreviewImage()}
          {hasContent ? (
            <button className="create-post-button" onClick={this.clickedSubmit}>
              Create Post
            </button>
          ) : null}
        </form>
      );
    } else return <h3>not logged in so hiding create post form</h3>;
  };

  renderCreatePost = () => {
    return <> {this.shitcode()}</>;
  };

  render() {
    const { text, tags, photo, user, error, loading } = this.state;
    const { posts } = this.state;

    return (
      <>
        <div className="feed-container">
          <p className="error-message">{error}</p>
          <div className="create-post">{this.renderCreatePost()}</div>
          {loading ? <div className="loading">Loading...</div> : ""}
          <FeedHandler />
        </div>
        <div className="actions-feed">actions feed goes here.</div>
      </>
    );
  }
}
export default Home;
