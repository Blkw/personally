import React, { Component } from "react";
import { postsByTags } from "./apiTags";
import Posts from "../post/Posts";
import { like, unlike } from "../post/apiPost";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth";
import DefaultProfile from "../assets/images/avatar.png";
import PostActionsPopup from "../assets/components/PostActionsPopup";
import Comment from "../post/Comment";
import PostActions from "../assets/components/PostActions";
import Linkify from "linkifyjs/react";
import ReactTinyLink from "react-tiny-link";
import CustomVideo from "../assets/components/CustomVideo";
import CustomVideoMp4 from "../assets/components/CustomVideoMp4";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight as arrow } from "@fortawesome/free-solid-svg-icons";
import Masonry from "react-masonry-css";

const breakpointColumnsObj = {
  default: 4,
  1600: 3,
  1300: 2,
  900: 1
};

const text = {
  marginTop: "0",
  marginBottom: "0"
};

const endOfFeed = {
  textAlign: "center",
  width: "100%",
  color: "lightGrey"
};

const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};
const postImage = {
  width: "100%",
  height: "auto",
  borderRadius: "10px",
  margin: "0"
};
export default class TagResults extends Component {
  constructor() {
    super();
    this.state = {
      posts: [],
      error: "",
      like: false,
      likes: 0,
      anyPosts: true,
      page: 1,
      searchTerm: ""
    };
  }

  componentDidMount = () => {
    let page = this.state.page;
    const { searchTerm } = this.props.match.params;
    this.setState({ searchTerm: searchTerm });

    postsByTags(searchTerm, page).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ posts: data });
        console.log(data);
      }
    });
  };
  componentWillReceiveProps = nextProps => {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      window.location.reload();
    }
  };

  loadMore = number => {
    this.setState({ page: this.state.posts.length + number });
    this.pushFeed(this.state.page + number);
  };

  pushFeed = page => {
    const searchTerm = this.state.searchTerm;
    postsByTags(searchTerm, page).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        if (!(data.length <= 0)) {
          var merged = [...this.state.posts, ...data];
          this.setState({
            posts: merged
          });
        } else {
          this.setState({ anyPosts: false });
          console.log("no more posts left to load :(");
        }
      }
    });
  };

  dateTime = dateTime => {
    //format todays date
    var today = new Date();
    today = moment(today, "YYYY-MM-DD-HH-SS").format();
    dateTime = moment(dateTime, "YYYY-MM-DD-HH-SS").format();
    console.log("today:", today);
    console.log("old  :", dateTime);

    //calculate
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = new Date(today - dateTime);

    console.log("Elapsed: ", elapsed);

    // console.log("formatted today   : ", formatted_today);

    if (elapsed < msPerMinute) {
      return Math.round(elapsed / 1000) + " seconds ago";
    } else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + " minutes ago";
    } else if (elapsed < msPerDay) {
      return Math.round(elapsed / msPerHour) + " hours ago";
    } else if (elapsed < msPerMonth) {
      return "approximately " + Math.round(elapsed / msPerDay) + " days ago";
    } else if (elapsed < msPerYear) {
      return (
        "approximately " + Math.round(elapsed / msPerMonth) + " months ago"
      );
    } else {
      return "approximately " + Math.round(elapsed / msPerYear) + " years ago";
    }
  };

  seeMore = post => {
    if (post.text.length > 400) {
      return (
        <Link className="comments-error" to={`/post/${post._id}`}>
          <br />
          <br />
          See more...
        </Link>
      );
    }
  };

  renderLinkPreview = text => {
    var matches = text.match(/(((https?:\/\/)|(www\.))[^\s]+)/g);

    if (matches != null) {
      var thisLink = matches[0];

      if (thisLink.search(/^http[s]?\:\/\//) === -1) {
        thisLink = "http://" + thisLink;
      }

      var hasyoutube = thisLink.includes("youtube");
      var hasVimeo = thisLink.includes("vimeo");
      var notFriendly = thisLink.includes(".mp4");

      if (hasyoutube || hasVimeo) {
        return <CustomVideo thisLink={thisLink} />;
      } else if (notFriendly) {
        return <CustomVideoMp4 thisLink={thisLink} />;
      } else {
        return (
          <ReactTinyLink
            cardSize="large"
            showGraphic={true}
            maxLine={3}
            minLine={1}
            url={`${thisLink}`}
          />
        );
      }
    } else return false;
  };

  renderPosts = posts => {
    return (
      <>
        <h3 className="react-tab-headers">
          Posts relating to: {this.state.searchTerm}
        </h3>
        <Masonry
          breakpointCols={breakpointColumnsObj}
          className="my-masonry-grid"
          columnClassName="my-masonry-grid_column"
        >
          {posts.map((post, i) => {
            const posterId = post.postedBy ? post.postedBy._id : "";
            const posterName = post.postedBy
              ? post.postedBy.username
              : "Unknown";
            return (
              <div key={i} className="feed-post search">
                <div className="author-section">
                  <Link to={`/user/${posterId}`}>
                    <div className="circle-photo-list">
                      <img
                        style={profileImage}
                        src={`${process.env.REACT_APP_API_URL}/user/photo/${posterId}`}
                        onError={i => (i.target.src = `${DefaultProfile}`)}
                        alt={posterName}
                      />
                    </div>
                  </Link>
                  <Link to={`/user/${posterId}`}>
                    <p className="poster">{posterName}</p>
                  </Link>
                  <p className="onto">
                    <FontAwesomeIcon icon={arrow} />
                  </p>
                  <Link to={`/user/${posterId}`}>
                    <p className="poster">{post.postsFeed.title}</p>
                  </Link>
                  {/* {this.dateTime(post.created)} */}
                </div>
                <div className="post-content">
                  <Link to={`/post/${post._id}`}>
                    <img
                      alt={post.text}
                      style={postImage}
                      src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
                      onError={i => (i.target.style.display = "none")}
                    />
                  </Link>
                  <p className="list-group-item-heading" style={text}>
                    <Linkify>{post.text.substring(0, 400)}</Linkify>
                    {/*  makes the maxiumum text on the feed 400 chars */}
                  </p>
                  {this.renderLinkPreview(post.text)}
                  {this.seeMore(post)}
                  <p className="list-group-item-text">
                    {this.renderTags(post)}
                  </p>
                </div>
              </div>
            );
          })}
        </Masonry>
        {this.loadMoreCheck(posts.length)}
      </>
    );
  };

  renderTags = post => {
    if (post.tags[0]) {
      var tagsArray = post.tags[0].split(",");
      console.log(tagsArray);
      return (
        <>
          {tagsArray.map((tag, i) => {
            return (
              <Link to={`/posts/search/${tag}`} key={i}>
                #{tag}
              </Link>
            );
          })}
        </>
      );
    } else {
      return null;
    }
  };

  loadMoreCheck = posts => {
    if (this.state.anyPosts && this.state.posts.length > 19) {
      return (
        <button
          className="load-more-button-search"
          onClick={() => this.loadMore(posts)}
        >
          Load more
        </button>
      );
    } else {
      return (
        <h5 style={endOfFeed}>
          No more posts could be found relating to: {this.state.searchTerm}
        </h5>
      );
    }
  };

  render() {
    const { posts } = this.state;
    return (
      <div className="general-container-search">{this.renderPosts(posts)}</div>
    );
  }
}
