export const postsByTags = (searchTerm, page) => {
  return fetch(
    `${process.env.REACT_APP_API_URL}/posts/search/${searchTerm}/?page=${page}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }
  )
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};

export const usersByTags = (searchTerm, page) => {
  return fetch(
    `${process.env.REACT_APP_API_URL}/users/search/${searchTerm}/?page=${page}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }
  )
    .then(response => {
      return response.json();
    })
    .catch(err => console.log(err));
};
