import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { create } from "./apiPost";
import { Redirect } from "react-router-dom";

class NewPost extends Component {
  constructor() {
    super();
    this.state = {
      text: null,
      tags: null,
      photo: null,
      error: null,
      user: {},
      fileSize: 0,
      loading: false
    };
  }

  handleChange = name => event => {
    this.setState({ error: "" });
    const value = name === "photo" ? event.target.files[0] : event.target.value;

    const fileSize = name === "photo" ? event.target.files[0].size : 0;
    this.postData.set(name, value);
    this.setState({ [name]: value, fileSize });
  };

  clickedSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });
    if (this.isValid()) {
      //send request to API
      const userId = isAuthenticated().user._id;
      const token = isAuthenticated().token;

      create(userId, token, this.postData).then(data => {
        //   if the data contains an error, set the state
        if (data.error) {
          this.setState({ error: data.error });
        } else {
          console.log("new post : ", data);
        }
      });
    }
  };

  componentDidMount() {
    this.postData = new FormData();
    this.setState({ user: isAuthenticated().user });
  }

  isValid = () => {
    const { text } = this.state;

    if (text.length < 5) {
      this.setState({
        error: "To make a post you need to use more than 5 characters."
      });
      return false;
    }

    return true;
  };

  newPostForm = (text, tags) => (
    <form>
      <div className="form-edit-profile-container">
        <div className="form-group">
          <label className="text-muted">picture</label>
          <input
            onChange={this.handleChange("photo")}
            type="file"
            accept="image/*"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="text-muted">text</label>
          <input
            onChange={this.handleChange("text")}
            type="text"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label className="text-muted">tags</label>
          <input
            onChange={this.handleChange("tags")}
            type="text"
            className="form-control"
          />
        </div>

        <button onClick={this.clickedSubmit} className="btn-primary">
          Create Post
        </button>
      </div>
    </form>
  );

  render() {
    const { text, tags, photo, user, error, loading } = this.state;

    // if (redirectToProfile) {
    //   return <Redirect to={`/user/${id}`} />;
    // }

    return (
      <div id="profile-container">
        <h2>Create new post</h2>
        <div
          className="alert alert-danger"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>
        {this.newPostForm(text, tags)}
        {loading ? <div className="loading">Loading...</div> : ""}
      </div>
    );
  }
}
export default NewPost;
