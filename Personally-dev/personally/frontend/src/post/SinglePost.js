import React, { Component } from "react";
import { singlePost, remove, like, unlike } from "./apiPost";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth";
import DefaultProfile from "../assets/images/avatar.png";
import PostActionsPopup from "../assets/components/PostActionsPopup";
import Comment from "./Comment";
import PostActions from "../assets/components/PostActions";
import Linkify from "linkifyjs/react";
import ReactTinyLink from "react-tiny-link";
import CustomVideo from "../assets/components/CustomVideo";
import CustomVideoMp4 from "../assets/components/CustomVideoMp4";

const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};
const postImage = {
  width: "100%",
  height: "auto",
  borderRadius: "10px",
  margin: "0"
};

const text = {
  marginTop: "0",
  marginBottom: "0"
};

const tags = {
  marginTop: "15px",
  marginBottom: "0",
  color: "#D2D7DC"
};

class SinglePost extends Component {
  state = {
    post: "",
    redirectToHome: false,
    redirectToSignin: false,
    comments: []
  };

  componentDidMount = () => {
    const postId = this.props.match.params.postId;
    singlePost(postId).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ post: data, comments: data.comments });
        console.log(data.comments);
      }
    });
  };

  deletePost = () => {
    const postId = this.props.match.params.postId;
    const token = isAuthenticated().token;
    remove(postId, token).then(data => {
      if (data.error) {
        console.log(data.error);
      } else {
        this.setState({ redirectToHome: true });
      }
    });
  };

  deleteConfirmed = () => {
    let answer = window.confirm("Are you sure you want to delete your post?");
    if (answer) {
      this.deletePost();
    }
  };

  updateComments = comments => {
    this.setState({ comments });
  };

  renderLinkPreview = text => {
    var matches = text.match(/(((https?:\/\/)|(www\.))[^\s]+)/g);

    if (matches != null) {
      var thisLink = matches[0];

      if (thisLink.search(/^http[s]?\:\/\//) == -1) {
        thisLink = "http://" + thisLink;
      }

      var hasyoutube = thisLink.includes("youtube");
      var hasVimeo = thisLink.includes("vimeo");
      var notFriendly = thisLink.includes(".mp4");

      if (hasyoutube || hasVimeo) {
        return <CustomVideo thisLink={thisLink} />;
      } else if (notFriendly) {
        return <CustomVideoMp4 thisLink={thisLink} />;
      } else {
        return (
          <ReactTinyLink
            cardSize="large"
            showGraphic={true}
            maxLine={3}
            minLine={1}
            url={`${thisLink}`}
          />
        );
      }
    } else return false;
  };

  renderPost = post => {
    const posterId = post.postedBy ? post.postedBy._id : "";
    const posterName = post.postedBy ? post.postedBy.username : " Unknown";

    const { comments } = this.state;

    return (
      <>
        <Link to={`/`} className="">
          Back to posts
        </Link>
        <div className="feed-post">
          <div className="author-section">
            <Link to={`/user/${posterId}`}>
              <div className="circle-photo-list">
                <img
                  style={profileImage}
                  src={`${
                    process.env.REACT_APP_API_URL
                  }/user/photo/${posterId}`}
                  onError={i => (i.target.src = `${DefaultProfile}`)}
                  alt={posterName}
                />
              </div>
            </Link>
            <Link to={`/user/${posterId}`}>
              <p className="poster">{posterName}</p>
            </Link>
            {/* {this.dateTime(post.created)} */}
            <PostActionsPopup post={post} />
          </div>
          <div className="post-content">
            <Link to={`/post/${post._id}`}>
              <img
                style={postImage}
                src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}`}
                onError={i => (i.target.style.display = "none")}
              />
            </Link>
            <p className="list-group-item-heading" style={text}>
              <Linkify>{post.text}</Linkify>
            </p>
            {this.renderLinkPreview(post.text)}
            <p className="list-group-item-text" style={tags}>
              #{post.tags}
            </p>
          </div>
          <PostActions post={post} />
        </div>

        <p className="font-italic mark">
          Posted by <Link to={`${posterId}`}>{posterName} </Link>
          on {new Date(post.created).toDateString()}
        </p>
      </>
    );
  };

  render() {
    const { post, redirectToHome, redirectToSignin } = this.state;

    if (redirectToHome) {
      return <Redirect to={`/`} />;
    } else if (redirectToSignin) {
      return <Redirect to={`/signin`} />;
    }

    return (
      <div className="general-container">
        {!post ? (
          <div className="jumbotron text-center">
            <h2>Loading...</h2>
          </div>
        ) : (
          this.renderPost(post)
        )}
      </div>
    );
  }
}

export default SinglePost;
