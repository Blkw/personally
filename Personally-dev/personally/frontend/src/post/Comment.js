import React, { Component } from "react";
import { comment, uncomment } from "./apiPost";
import { isAuthenticated } from "../auth";
import { Link } from "react-router-dom";
import DefaultProfile from "../assets/images/avatar.png";
import TextareaAutosize from "react-textarea-autosize";
import Linkify from "linkifyjs/react";
import { Button, Popup, Grid } from "semantic-ui-react";
import DeleteMessage from "../assets/components/DeleteMessage";

//styling
const cardStyle = {
  borderBottom: "1px solid lightgrey"
};

const profileImage = {
  width: "45px",
  borderRadius: "100px",
  margin: "0"
};

const pFix = {
  marginTop: "0",
  marginBottom: "0",
  color: "#52606F",
  fontWeight: "600"
};

const popupStyle = {
  backgroundColor: "white",
  padding: "0px 5px 0px 5px",
  boxShadow: "0px 0px 10px -5px lightgrey",
  border: "solid 1px #D2D7DC",
  borderRadius: "10px 10px 10px 0px"
};

const gridRowLast = {
  paddingBottom: "3px",
  paddingTop: "0px",
  cursor: "pointer"
};

export default class Comment extends Component {
  state = {
    text: "",
    error: "",
    showComponent: false
  };

  deletePrompt = () => {
    this.setState({
      showComponent: true
    });
    console.log("DeletePrompt called");
  };

  isValid = () => {
    const { text } = this.state;

    if (!text.length > 0) {
      this.setState({ error: "Comments cannot be empty." });
      return false;
    }
    return true;
  };

  handleChange = event => {
    this.setState({ error: "" });
    this.setState({ text: event.target.value });
  };

  addComment = event => {
    if (!isAuthenticated()) {
      this.setState({
        error: "Please signin or create an account to make a comment"
      });
      return false;
    }
    if (this.isValid()) {
      const userId = isAuthenticated().user._id;
      const token = isAuthenticated().token;
      const postId = this.props.postId;

      comment(userId, token, postId, { text: this.state.text }).then(data => {
        if (data.error) {
          console.log(data.error);
        } else {
          this.setState({ text: "" });
          this.props.updateComments(data.comments);
          this.props.updateNotes(this.props.notes + 1);
        }
      });
    }
  };

  checkCommentsLength = () => {
    const { postId } = this.props;
    const { comments } = this.props;

    if (comments.length > 5) {
      return (
        <Link className="comments-error" to={`/post/${postId}`}>
          {" "}
          See more comments...
        </Link>
      );
    }
    return false;
  };

  onEnterPress = e => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();
      this.addComment();
    }
  };

  deleteComment = comment => {
    if (comment.postedBy._id == isAuthenticated().user._id) {
      return (
        <>
          <Popup
            trigger={<Button className="comment-tridot-button">...</Button>}
            flowing
            on="click"
          >
            <Grid centered divided columns={1} style={popupStyle}>
              <Grid.Column textAlign="left">
                <Grid.Row>
                  <p style={gridRowLast} onClick={this.deletePrompt}>
                    delete
                  </p>
                  {this.state.showComponent ? (
                    <DeleteMessage
                      title={"Delete comment?"}
                      message={"Are you sure you want to delete this comment?"}
                      type={"comment"}
                      comment={comment}
                    />
                  ) : null}
                </Grid.Row>
              </Grid.Column>
            </Grid>
          </Popup>
        </>
      );
    } else {
      return null;
    }
  };

  shitcode = () => {
    const { comments } = this.props;
    return (
      <>
        <ul className="list-group-comments">
          {comments.map((comment, i) => (
            <div key={i} className="list-group-item-inline-comments">
              <Link
                to={`/user/${comment.postedBy._id}`}
                style={cardStyle}
                key={i}
              >
                <div className="circle-photo-list-small">
                  <img
                    style={profileImage}
                    src={`${process.env.REACT_APP_API_URL}/user/photo/${comment.postedBy._id}`}
                    onError={i => (i.target.src = `${DefaultProfile}`)}
                    alt={comment.postedBy.username}
                  />
                </div>
              </Link>
              <div className="user-info">
                <Link
                  to={`/user/${comment.postedBy._id}`}
                  style={cardStyle}
                  key={i}
                >
                  <p className="list-group-item-heading" style={pFix}>
                    {comment.postedBy.username}
                  </p>
                </Link>
                <div className="comment-container">
                  <p className="list-group-item-text" className="comment">
                    <Linkify>{comment.text}</Linkify>
                  </p>
                  {this.deleteComment(comment)}
                </div>
              </div>
            </div>
          ))}
        </ul>
      </>
    );
  };
  render() {
    const { comments } = this.props;
    const { error } = this.state;

    return (
      <div>
        <form onSubmit={this.addComment} ref={e => (this.myFormRef = e)}>
          {/* <input
            type="text"
            onChange={this.handleChange}
            className="make-comment"
            placeholder="Write a comment..."
            value={this.state.text}
          /> */}
          <TextareaAutosize
            className="make-comment"
            placeholder="Write a comment..."
            type="text"
            onChange={this.handleChange}
            value={this.state.text}
            onKeyDown={this.onEnterPress}
          />
        </form>
        <div
          className="comments-error"
          style={{ display: error ? "" : "none" }}
        >
          {error}
        </div>
        {comments ? this.shitcode() : "Loading comments..."}
      </div>
    );
  }
}
