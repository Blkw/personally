const _ = require("lodash");
const Post = require("../models/post");
const Feed = require("../models/feed");
const User = require("../models/user");

const formidable = require("formidable");
const fs = require("fs");

exports.postById = (req, res, next, id) => {
  Post.findById(id)
    .populate("postedBy", "_id username")
    .populate("postsFeed", "_id title")
    .populate("comments.postedBy", "_id username")
    .select("_id text tags created comments photo likes notes")
    .exec((err, post) => {
      if (err || !post) {
        return res.status(400).json({
          error: err
        });
      }

      //only gets 5 comments
      post.comments = post.comments.slice(0, 5);

      req.post = post;
      next();
    });
};

exports.getPosts = (req, res) => {
  var posts = Post.find()
    .populate("postedBy", "_id username")
    .populate("comments", "text created")
    .populate("postsFeed", "_id title")
    .populate("comments.postedBy", "_id username")
    .select("_id text tags created comments likes notes")
    .sort({ created: -1 })
    .then(posts => {
      res.json(posts);
    })
    .catch(err => console.log(err));
};

exports.postsForTimeline = (req, res) => {
  // const feedId = req.newsFeeds.following;

  //adds this.users posts to the request

  var newsfeedOne = req.profile.newsFeeds[0].following;

  if (req.profile.newsFeeds[1]) {
    var newsfeedTwo = req.profile.newsFeeds[1].following;
  }
  if (req.profile.newsFeeds[2]) {
    var newsfeedThree = req.profile.newsFeeds[2].following;
  }
  if (req.profile.newsFeeds[3]) {
    var newsfeedFour = req.profile.newsFeeds[3].following;
  }
  if (req.profile.newsFeeds[4]) {
    var newsfeedFive = req.profile.newsFeeds[4].following;
  }

  var allNewsfeeds = newsfeedOne;

  if (newsfeedTwo) {
    allNewsfeeds = newsfeedOne.concat(newsfeedTwo);
  }
  if (newsfeedThree) {
    allNewsfeeds = allNewsfeeds.concat(newsfeedThree);
  }
  if (newsfeedFour) {
    allNewsfeeds = allNewsfeeds.concat(newsfeedFour);
  }
  if (newsfeedFive) {
    allNewsfeeds = allNewsfeeds.concat(newsfeedFive);
  }

  //add this.users posts
  allNewsfeeds.push(req.profile._id);

  Post.find({ postedBy: { $in: allNewsfeeds } })
    .populate("comments", "text created")
    .populate("comments.postedBy", "_id username")
    .populate("postedBy", "_id username")
    .populate("postsFeed", "_id title")

    .sort("-created")
    .exec((err, postsFromFeeds) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        });
      }
      Post.find({ postsFeed: { $in: allNewsfeeds } })
        .populate("comments", "text created")
        .populate("comments.postedBy", "_id username")
        .populate("postedBy", "_id username")
        .populate("postsFeed", "_id title")

        .sort("-created")
        .exec((err, posts) => {
          if (err) {
            console.log(err);
            return res.status(400).json({
              error: errorHandler.getErrorMessage(err)
            });
          }

          var finalObj = posts.concat(postsFromFeeds);

          finalObj.sort((a, b) =>
            a.created > b.created ? 1 : b.created > a.created ? -1 : 0
          );
          filtered = uniqBy(finalObj, JSON.stringify);
          filtered.reverse();

          const numberOfPosts = req.query.page - 1;

          var nextPage = numberOfPosts + 4;

          console.log(numberOfPosts, "AND ", nextPage);

          filtered = filtered.slice(numberOfPosts, nextPage);

          res.json(filtered);
        });
    });
};

//removes duplicates ( if someone follows the user and also a feed they make, itll only show the post once)
function uniqBy(a, key) {
  var seen = {};
  return a.filter(function(item) {
    var k = key(item);
    return seen.hasOwnProperty(k) ? false : (seen[k] = true);
  });
}
exports.createPost = (req, res, next) => {
  console.log("Req profile: \n\n", req.profile, "\n\n");

  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Image could not be uploaded"
      });
    }
    let post = new Post(fields);

    req.profile.hashed_password = undefined;
    req.profile.salt = undefined;

    post.postedBy = req.profile;
    post.postsFeed = req.feed;
    console.log("Posts Feed value: \n\n", post.postsFeed, "\n\n");

    if (files.photo) {
      post.photo.data = fs.readFileSync(files.photo.path);
      post.photo.contentType = files.photo.type;
    }

    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      Feed.findByIdAndUpdate(
        post.postsFeed,
        { $push: { posts: post.postsFeed._id } },
        { new: true }
      );
      res.json(result);
    });
  });
};

exports.singlePost = (req, res) => {
  return res.json(req.post);
};

exports.photo = (req, res, next) => {
  res.set("Content-Type", req.post.photo.contentType);
  return res.send(req.post.photo.data);
};
exports.postsByUser = (req, res) => {
  Post.find({ postedBy: req.profile._id })
    .populate("postedBy", "_id username")
    .populate("comments", "text created")
    .populate("comments.postedBy", "_id username")
    .populate("postsFeed", "_id title")

    .sort({ created: -1 })
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(posts);
    });
};
exports.isPoster = (req, res, next) => {
  let isPoster = req.post && req.auth && req.post.postedBy._id == req.auth._id;
  if (!isPoster) {
    return status(400).json({
      error: "user is not authorized."
    });
  }
  next();
};

exports.updatePost = (req, res, next) => {
  let form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Photo could not be uploaded"
      });
    }
    // save post
    let post = req.post;
    post = _.extend(post, fields);
    post.updated = Date.now();

    if (files.photo) {
      post.photo.data = fs.readFileSync(files.photo.path);
      post.photo.contentType = files.photo.type;
    }

    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(post);
    });
  });
};

exports.deletePost = (req, res) => {
  let post = req.post;
  post.remove((err, post) => {
    if (err) {
      return res.status(400).json({
        error: err
      });
    }
    res.json({
      message: "Post deleted."
    });
  });
};

exports.postPhoto = (req, res, next) => {
  res.set("Content-Type", req.post.photo.contentType);
  return res.send(req.post.photo.data);
};

exports.like = (req, res) => {
  Post.findByIdAndUpdate(
    req.body.postId,
    { $push: { likes: req.body.userId } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: err
      });
    } else {
      Post.findByIdAndUpdate(req.body.postId, { $inc: { notes: 1 } }).exec(
        (err, result) => {
          if (err) {
            return res.status(400).json({
              error: err
            });
          } else {
            res.json(result);
          }
        }
      );
    }
  });
};

exports.unlike = (req, res) => {
  Post.findByIdAndUpdate(
    req.body.postId,
    { $pull: { likes: req.body.userId } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: err
      });
    } else {
      Post.findByIdAndUpdate(req.body.postId, { $inc: { notes: -1 } }).exec(
        (err, result) => {
          if (err) {
            return res.status(400).json({
              error: err
            });
          } else {
            res.json(result);
          }
        }
      );
    }
  });
};

exports.comment = (req, res) => {
  let comment = req.body.comment;
  comment.postedBy = req.body.userId;

  Post.findByIdAndUpdate(
    req.body.postId,
    { $push: { comments: comment } },
    { new: true }
  )
    .populate("comments.postedBy", "_id username")
    .populate("postedBy", "_id username")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      } else {
        Post.findByIdAndUpdate(req.body.postId, { $inc: { notes: 1 } }).exec(
          (err, result) => {
            if (err) {
              return res.status(400).json({
                error: err
              });
            }
          }
        );
        res.json(result);
      }
    });
};

exports.uncomment = (req, res) => {
  let comment = req.body.comment;

  Post.findByIdAndUpdate(
    req.body.postId,
    { $pull: { comments: { _id: comment._id } } },
    { new: true }
  )
    .populate("comments.postedBy", "_id username")
    .populate("postedBy", "_id username")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      } else {
        Post.findByIdAndUpdate(req.body.postId, { $inc: { notes: 1 } }).exec(
          (err, result) => {
            if (err) {
              return res.status(400).json({
                error: err
              });
            }
          }
        );
        res.json(result);
      }
    });
};

exports.getComments = async (req, res) => {
  var postId = req.params.postId;
  var noComments = req.query.noComments;

  Post.findById(postId)
    .populate("comments.postedBy", "_id username")
    .select("comments")
    .select("-_id")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      } else {
        JSON.parse(JSON.stringify(result.comments));

        var nextPage = +noComments + 5;

        var comments = [];

        for (i = +noComments; i < nextPage; i++) {
          if (result.comments[i]) {
            comments.push(result.comments[i]);
          }
          console.log(result.comments[i]);
        }

        console.log(noComments, "AND ", nextPage);
        res.json(comments);
      }
    });
};

exports.getPostsFromTag = async (req, res) => {
  console.log(req.params.searchTerm);

  Post.find({ tags: { $regex: String(req.params.searchTerm) } })
    .populate("postedBy", "_id username")
    .populate("comments", "text created")
    .populate("postsFeed", "_id title")
    .populate("comments.postedBy", "_id username")
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }

      posts.sort((a, b) =>
        a.created > b.created ? 1 : b.created > a.created ? -1 : 0
      );
      posts = uniqBy(posts, JSON.stringify);
      posts.reverse();

      const numberOfPosts = req.query.page - 1;

      var nextPage = numberOfPosts + 20;

      console.log(numberOfPosts, "AND ", nextPage);

      posts = posts.slice(numberOfPosts, nextPage);

      res.json(posts);
    });
};
