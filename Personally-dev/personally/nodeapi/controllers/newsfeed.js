const Newsfeed = require("../models/newsfeed");
const Post = require("../models/post");
const User = require("../models/user");

const formidable = require("formidable");

exports.createNewsfeed = (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, (err, fields) => {
    if (err) {
      return res.status(400).json({
        error: "Image could not be uploaded"
      });
    }

    let newsfeed = new Newsfeed(fields);
    req.profile.hasedPassword = undefined;
    req.profile.salt = undefined;
    req.profile.photo = undefined;
    req.profile.coverPhoto = undefined;
    req.profile.followers = undefined;
    req.profile.following = undefined;
    req.profile.firstname = undefined;
    req.profile.lastname = undefined;
    req.profile.email = undefined;

    newsfeed.createdBy = req.profile;

    console.log("creating newsfeed with user: ", req.profile);

    User.findByIdAndUpdate(
      req.profile._id,
      { $push: { newsFeeds: newsfeed._id } },
      { new: true }
    ).exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      console.log("added newsfeed to user with ", result);
    });

    newsfeed.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.status(200).json({
        newsfeed: result
      });
    });
  });
};

exports.newsfeedById = (req, res, next, id) => {
  Newsfeed.findById(id)
    .populate("createdBy", "_id username")
    .select("_id title tags created following")
    .exec((err, newsfeed) => {
      if (err || !newsfeed) {
        return res.status(400).json({
          error: err
        });
      }
      req.newsfeed = newsfeed;
      next();
    });
};

exports.singleNewsfeed = (req, res) => {
  return res.json(req.newsfeed);
};

exports.getNewsfeeds = (req, res) => {
  var newsfeed = Newsfeed.find()
    .populate("createdBy", "_id username")
    .select("_id title tags created ")
    .sort({ created: -1 })
    .then(newsfeed => {
      res.json(newsfeed);
    })
    .catch(err => console.log(err));
};

exports.newsfeedsByUser = (req, res) => {
  Newsfeed.find({ createdBy: req.profile._id })
    .populate("createdBy", "_id username")
    .select("_id title tags following")

    .sort({ created: -1 })
    .exec((err, newsfeeds) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(newsfeeds);
    });
};

exports.postsByNewsfeed = async (req, res) => {
  const feedId = req.newsfeed.following;

  //adds this.users posts to the request
  feedId.push(req.newsfeed.createdBy._id);

  Post.find({
    postsFeed: { $in: feedId }
  })
    .populate("comments", "text created")
    .populate("likes", "_id")
    .populate("comments.postedBy", "_id username")
    .populate("postedBy", "_id username")
    .populate("postsFeed", "_id title")
    .sort("-created")
    .exec((err, postsFromFeeds) => {
      if (err) {
        console.log(err);
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        });
      }

      Post.find({
        postedBy: { $in: feedId }
      })
        .populate("comments", "text created")
        .populate("likes", "_id")
        .populate("comments.postedBy", "_id username")
        .populate("postedBy", "_id username")
        .populate("postsFeed", "_id title")
        .sort("-created")
        .exec((err, posts) => {
          if (err) {
            console.log(err);
            return res.status(400).json({
              error: errorHandler.getErrorMessage(err)
            });
          }

          var finalObj = posts.concat(postsFromFeeds);

          finalObj.sort((a, b) =>
            a.created > b.created ? 1 : b.created > a.created ? -1 : 0
          );
          filtered = uniqBy(finalObj, JSON.stringify);
          filtered.reverse();

          const numberOfPosts = req.query.page - 1;

          var nextPage = numberOfPosts + 4;

          console.log(numberOfPosts, "AND ", nextPage);

          filtered = filtered.slice(numberOfPosts, nextPage);
          res.json(filtered);
        });
    });
};

//removes duplicates ( if someone follows the user and also a feed they make, itll only show the post once)
function uniqBy(a, key) {
  var seen = {};
  return a.filter(function(item) {
    var k = key(item);
    return seen.hasOwnProperty(k) ? false : (seen[k] = true);
  });
}

//following / follow code
exports.addFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $push: { following: req.body.followId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};
//this isnt going to be used:

exports.addFollower = (req, res) => {
  Feed.findByIdAndUpdate(
    req.body.followId,
    { $push: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id title username firstname lastname")
    .populate("followers", "_id title username firstname lastname")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(result);
    });
};

// remove follow unfollow
exports.removeFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $pull: { following: req.body.unfollowId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};

exports.removeFollower = (req, res) => {
  Newsfeed.findByIdAndUpdate(
    req.body.unfollowId,
    { $pull: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id title username firstname lastname")
    .populate("followers", "_id title username firstname lastname")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      result.hashed_password = undefined;
      result.salt = undefined;
      res.json(result);
    });
};
