const _ = require("lodash");
const User = require("../models/user");
const Newsfeed = require("../models/newsfeed");
const formidable = require("formidable");
const fs = require("fs");

exports.userById = (req, res, next, id) => {
  User.findById(id)
    // populate followers and following users array
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname title")
    .populate("newsFeeds", "_id title following ")

    .exec((err, user) => {
      if (err || !user) {
        return res.status(400).json({
          error: err
        });
      }
      req.profile = user; // adds profile object in req with user info
      next();
    });
};

exports.hasAuthorzation = (req, res, next) => {
  const authorized =
    req.profile && req.auth && req.profile._id === req.auth._id;
  if (!authorized) {
    return res.status(403).json({
      error: "user is not authorized to perform this action."
    });
  }
};

exports.allUsers = (req, res) => {
  User.find((err, users) => {
    if (err) {
      return res.status(400).json({
        error: err
      });
    }
    res.json(users);
  })
    .select("username firstname lastname email updated created")
    .populate("newsFeeds", "_id title following ")
    .populate("feeds", "_id title");
};

exports.getUser = (req, res) => {
  req.profile.hasedPassword = undefined;
  req.profile.salt = undefined;
  return res.json(req.profile);
};

exports.userPhoto = (req, res, next) => {
  if (req.profile.photo.data) {
    res.set(("Content-Type", req.profile.photo.contentType));
    return res.send(req.profile.photo.data);
  }
  next();
};

exports.userCoverPhoto = (req, res, next) => {
  if (req.profile.coverPhoto.data) {
    res.set(("Content-Type", req.profile.coverPhoto.contentType));
    return res.send(req.profile.coverPhoto.data);
  }
  next();
};

exports.updateUser = (req, res, next) => {
  let form = new formidable.IncomingForm();
  // console.log("incoming form data: ", form);
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Photo could not be uploaded"
      });
    }
    // save user
    let user = req.profile;
    // console.log("user in update: ", user);
    user = _.extend(user, fields);

    user.updated = Date.now();
    // console.log("USER FORM DATA UPDATE: ", user);

    if (files.photo) {
      user.photo.data = fs.readFileSync(files.photo.path);
      user.photo.contentType = files.photo.type;
    }
    if (files.coverPhoto) {
      user.coverPhoto.data = fs.readFileSync(files.coverPhoto.path);
      user.coverPhoto.contentType = files.coverPhoto.type;
    }

    user.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      user.hashed_password = undefined;
      user.salt = undefined;
      // console.log("user after update with formdata: ", user);
      res.json(user);
    });
  });
};

exports.deleteUser = (req, res, next) => {
  let user = req.profile;
  //was users
  user.remove((err, user) => {
    if (err) {
      return res.status(400).json({
        error: err
      });
    }
    user.hasedPassword = undefined;
    user.salt = undefined;
    res.json({ message: "User deleted successfully." });
  });
};

//following / follow code
exports.addFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $push: { following: req.body.followId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};

exports.addFollower = (req, res) => {
  User.findByIdAndUpdate(
    req.body.followId,
    { $push: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      result.hashed_password = undefined;
      result.salt = undefined;
      res.json(result);
    });
};

// remove follow unfollow
exports.removeFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $pull: { following: req.body.unfollowId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};

exports.removeFollower = (req, res) => {
  User.findByIdAndUpdate(
    req.body.unfollowId,
    { $pull: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      result.hashed_password = undefined;
      result.salt = undefined;
      res.json(result);
    });
};

exports.getUsersfromTag = async (req, res) => {
  console.log(req.params.searchTerm);

  User.find({ username: { $regex: String(req.params.searchTerm) } })
    .populate("postedBy", "_id username")
    .exec((err, users) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }

      users.sort((a, b) =>
        a.created > b.created ? 1 : b.created > a.created ? -1 : 0
      );
      users = uniqBy(users, JSON.stringify);
      users.reverse();

      const numberOfUsers = req.query.page - 1;

      var nextPage = numberOfUsers + 20;

      console.log(numberOfUsers, "AND ", nextPage);

      v = users.slice(numberOfPosts, nextPage);

      res.json(users);
    });
};
