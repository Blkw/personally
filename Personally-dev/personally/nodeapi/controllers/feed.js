const Feed = require("../models/feed");
const Post = require("../models/post");
const Newsfeed = require("../models/newsfeed");

const formidable = require("formidable");

exports.createFeed = (req, res) => {
  const form = new formidable.IncomingForm();
  form.parse(req, (err, fields) => {
    if (err) {
      return res.status(400).json({
        error: "Image could not be uploaded"
      });
    }

    let feed = new Feed(fields);
    req.profile.hasedPassword = undefined;
    req.profile.salt = undefined;
    req.profile.photo = undefined;
    req.profile.coverPhoto = undefined;
    req.profile.followers = undefined;
    req.profile.following = undefined;
    req.profile.firstname = undefined;
    req.profile.lastname = undefined;
    req.profile.email = undefined;

    feed.createdBy = req.profile;

    console.log("creating feed: ", req.body);

    feed.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.status(200).json({
        feed: result
      });
    });
  });
};

exports.feedById = (req, res, next, id) => {
  Feed.findById(id)
    .populate("createdBy", "_id username")
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname")
    .select("_id title tags created posts")
    .exec((err, feed) => {
      if (err || !feed) {
        return res.status(400).json({
          error: err
        });
      }
      req.feed = feed;
      next();
    });
};

exports.singleFeed = (req, res) => {
  return res.json(req.feed);
};

exports.getFeeds = (req, res) => {
  var feed = Feed.find()
    .populate("createdBy", "_id username")
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname")
    .select("_id title tags created posts")
    .sort({ created: -1 })
    .then(feed => {
      res.json(feed);
    })
    .catch(err => console.log(err));
};

exports.feedsByUser = (req, res) => {
  Feed.find({ createdBy: req.profile._id })
    .populate("createdBy", "_id username")
    .sort({ created: -1 })
    .exec((err, feeds) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(feeds);
    });
};

exports.postsByFeed = (req, res) => {
  console.log("\n Printing from postsByFeed - ID : \n\n ", req.feed._id, " \n");

  Post.find({ postsFeed: req.feed._id })
    .populate("postedBy", "_id username")
    .populate("post", "_id text tags photo postsFeed")
    .populate("postsFeed", "_id title")
    .sort({ created: -1 })
    .exec((err, posts) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(posts);
    });
};

//following / follow code
exports.addFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $push: { following: req.body.followId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};

exports.addFollower = (req, res) => {
  Feed.findByIdAndUpdate(
    req.body.followId,
    { $push: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id title")
    .populate("followers", "_id title ")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      res.json(result);
    });
};

// remove follow unfollow
exports.removeFollowing = (req, res, next) => {
  Newsfeed.findByIdAndUpdate(
    req.body.newsfeedId,
    { $pull: { following: req.body.unfollowId } },
    (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }
      next();
    }
  );
};

exports.removeFollower = (req, res) => {
  Feed.findByIdAndUpdate(
    req.body.unfollowId,
    { $pull: { followers: req.body.newsfeedId } },
    { new: true }
  )
    .populate("following", "_id username firstname lastname")
    .populate("followers", "_id username firstname lastname")
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        });
      }
      result.hashed_password = undefined;
      result.salt = undefined;
      res.json(result);
    });
};
