const express = require("express");
const {
  createNewsfeed,
  getNewsfeeds,
  newsfeedsByUser,
  singleNewsfeed,
  newsfeedById,
  postsByNewsfeed,
  addFollowing,
  addFollower,
  removeFollowing,
  removeFollower
} = require("../controllers/newsfeed");
const { requireSignin } = require("../controllers/auth");
const { createNewsfeedValidator } = require("../validator");
const { userById } = require("../controllers/user");
const router = express.Router();

//following
router.put("/newsfeed/follow", requireSignin, addFollowing, addFollower);
router.put(
  "/newsfeed/unfollow",
  requireSignin,
  removeFollowing,
  removeFollower
);

router.post(
  "/newsfeed/new/:userId",
  requireSignin,
  createNewsfeed,
  createNewsfeedValidator
);

router.get("/newsfeeds", getNewsfeeds);

router.get("/newsfeed/:newsfeedId", singleNewsfeed);

router.get("/newsfeeds/by/:userId", newsfeedsByUser);

router.get("/posts-by-newsfeed/:newsfeedId", postsByNewsfeed);

router.param("userId", userById);

// any routes containing :feedID, our app will first execute feedById()
router.param("newsfeedId", newsfeedById);

// router.put("/user/:userId", requireSignin, updateUser);
// router.delete("/user/:userId", requireSignin, deleteUser);

module.exports = router;
