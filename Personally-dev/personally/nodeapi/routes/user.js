const express = require("express");

const {
  userById,
  allUsers,
  getUser,
  updateUser,
  deleteUser,
  addFollowing,
  addFollower,
  removeFollower,
  removeFollowing,
  userPhoto,
  userCoverPhoto,
  getUsersFromTag
} = require("../controllers/user");

const { requireSignin } = require("../controllers/auth");

const router = express.Router();

//following
router.put("/user/follow", requireSignin, addFollowing, addFollower);
router.put("/user/unfollow", requireSignin, removeFollowing, removeFollower);

router.get("/users", allUsers);
router.get("/user/:userId", getUser);
// router.get("/users/search/:searchTerm", getUsersFromTag);

//user photo
router.get("/user/photo/:userId", userPhoto);
router.get("/user/coverPhoto/:userId", userCoverPhoto);

router.put("/user/:userId", requireSignin, updateUser);
router.delete("/user/:userId", requireSignin, deleteUser);

// any routes containing :userID, our app will first execute userById()
router.param("userId", userById);

module.exports = router;
