const express = require("express");
const {
  createFeed,
  getFeeds,
  feedsByUser,
  singleFeed,
  feedById,
  postsByFeed,
  addFollowing,
  addFollower,
  removeFollowing,
  removeFollower
} = require("../controllers/feed");
const { requireSignin } = require("../controllers/auth");
const { createFeedValidator } = require("../validator");
const { userById } = require("../controllers/user");
const router = express.Router();

//following
router.put("/feed/follow", requireSignin, addFollowing, addFollower);
router.put("/feed/unfollow", requireSignin, removeFollowing, removeFollower);

router.post(
  "/feed/new/:userId",
  requireSignin,
  createFeed,
  createFeedValidator
);

router.get("/feeds", getFeeds);

router.get("/feed/:feedId", singleFeed);

router.get("/feeds/by/:userId", feedsByUser);

router.get("/posts-by-feed/:feedId", postsByFeed);

router.param("userId", userById);

// any routes containing :feedID, our app will first execute feedById()
router.param("feedId", feedById);

// router.put("/user/:userId", requireSignin, updateUser);
// router.delete("/user/:userId", requireSignin, deleteUser);

module.exports = router;
