const express = require("express");
const {
  getPosts,
  createPost,
  postsByUser,
  postById,
  isPoster,
  updatePost,
  deletePost,
  postPhoto,
  singlePost,
  postsForTimeline,
  like,
  unlike,
  comment,
  uncomment,
  getComments,
  getPostsFromTag
} = require("../controllers/post");
const { requireSignin } = require("../controllers/auth");
const { userById } = require("../controllers/user");
const { feedById } = require("../controllers/feed");

const { createPostValidator } = require("../validator");

const router = express.Router();

router.get("/posts/:userId", postsForTimeline);
router.get("/posts/search/:searchTerm", getPostsFromTag);

//likes
router.put("/post/like", requireSignin, like);
router.put("/post/unlike", requireSignin, unlike);

//comments
router.put("/post/comment", requireSignin, comment);
// router.put("/post/uncomment", requireSignin, uncomment);

router.post(
  "/post/new/:userId/:feedId",
  requireSignin,
  createPost,
  createPostValidator
);

router.get("/posts/by/:userId", postsByUser);
router.get("/post/:postId", singlePost);
//get comments
router.get("/post-comments/:postId", getComments);

router.put("/post/:postId", requireSignin, isPoster, updatePost);
router.delete("/post/:postId", requireSignin, isPoster, deletePost);

// photo
router.get("/post/photo/:postId", postPhoto);

// any routes containing :userID, our app will first execute userById()
router.param("userId", userById);

// any routes containing :feedID, our app will first execute postById()
router.param("feedId", feedById);
// any routes containing :postID, our app will first execute postById()
router.param("postId", postById);

module.exports = router;
