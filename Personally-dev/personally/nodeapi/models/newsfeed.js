const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const newsfeedSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true
  },
  tags: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: Date,
  NSFW: Boolean,
  createdBy: {
    type: ObjectId,
    ref: "User"
  },
  posts: [{ type: ObjectId, ref: "Post" }],
  following: [{ type: ObjectId, ref: "Feed" }],
  followers: [{ type: ObjectId, ref: "User" }]
});

module.exports = mongoose.model("Newsfeed", newsfeedSchema);
