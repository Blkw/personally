const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const postSchema = new mongoose.Schema({
  text: {
    type: String,
    required: "content is required to make a post.",
    minLength: 4,
    maxlength: 2000
  },
  tags: [
    {
      type: String
    }
  ],
  photo: {
    data: Buffer,
    contentType: String
  },
  postedBy: {
    type: ObjectId,
    ref: "User"
  },
  postsFeed: {
    type: ObjectId,
    ref: "Feed"
  },
  created: {
    type: Date,
    default: Date.now
  },
  likes: [{ type: ObjectId, ref: "User" }],
  comments: [
    {
      text: String,
      created: { type: Date, default: Date.now },
      postedBy: { type: ObjectId, ref: "User" }
    }
  ],
  notes: {
    type: Number,
    min: 0,
    default: 0
  }
});

module.exports = mongoose.model("Post", postSchema);
